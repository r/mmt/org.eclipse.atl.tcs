action auto-bind(comp) {
  // Selects the interfaces to connect
  clients = $comp/interface::*[required(.)][not(bound(.))];
  for itf : $clients {
    // Search for candidates compatible interfaces
    candidates = $comp/sibling::*/interface::*[compatible($itf, .)];
    if (not(empty($candidates))) {
      // Connect one of these candidates
      bind($itf, one-of($candidates));
    }
  }
  return size($comp/interface::*[required(.)][not(bound(.))]) == 0;
}
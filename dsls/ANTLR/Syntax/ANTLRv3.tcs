-- @authors		Fr�d�ric Jouault
-- @date		2007/09/19
-- @description	This TCS model defines the syntax of the ANTLRv3 language.

-- REMARK: this model can only be used for serialization so far.
-- NOT TO BE USED as a typical example of TCS model.
syntax ANTLR {

	primitiveTemplate identifier for String default using NAME:
		value = "%token%";

	primitiveTemplate stringSymbol for String using STRING:
		value = "%token%",
		serializer="'\'' + %value%.toCString() + '\''";

	primitiveTemplate integerSymbol for Integer default using INT:
		value = "Integer.valueOf(%token%)";

	primitiveTemplate booleanSymbol for Boolean default using BOOLEAN:
		value = "Boolean.valueOf(%token%)";

	template Grammar main
		:
			"grammar" name <no_space> "_ANTLR3" ";"
			(isDefined(options) ?
				"options" "{" [
					options
				] "}"
			)
			(isDefined(package) ?
				"@header" "{"
					"package" package ";"
				"}"
				"@lexer::header" "{"
					"package" package ";"
				"}"
"@lexer::members {

	private void newline() {}

	public org.eclipse.gmt.tcs.injector.TCSRuntime ei = null;

	public void displayRecognitionError(String[] tokenNames, RecognitionException e) {
		ei.reportError((Exception)e);
	}

	public Token emit() {
		org.eclipse.gmt.tcs.injector.wrappers.antlr3.ANTLR3LocationToken ret = null;

		ret = new org.eclipse.gmt.tcs.injector.wrappers.antlr3.ANTLR3LocationToken(input, type, channel, tokenStartCharIndex, getCharIndex()-1);
		ret.setLine(tokenStartLine);
		ret.setText(text);
	  	ret.setCharPositionInLine(tokenStartCharPositionInLine);
		ret.setEndLine(getLine());
		ret.setEndColumn(getCharPositionInLine());
		emit(ret);

		return ret;
	}

}"
			)
			(isDefined(actions) ?
				"@members" "{" [
					actions
					"public static void main(String[] args) throws Exception {
						CharStream input = new ANTLRFileStream(args[0]);"
						name <no_space> "_ANTLR3Lexer lex = new " name <no_space> "_ANTLR3Lexer(input);
						CommonTokenStream tokens = new CommonTokenStream(lex);
						tokens.discardTokenType(WS);
						tokens.discardTokenType(COMMENT);
						tokens.discardTokenType(NL);
						"
						name <no_space> "_ANTLR3Parser parser = new " name <no_space> "_ANTLR3Parser(tokens);
						parser.main();
					}"
				] "}"
			)
			[
				rules
			] {indentIncr = 0, nbNL = 2}
			(isDefined('lexer') ?
				'lexer'
				[ lexerRules ] {indentIncr = 0, nbNL = 2}
			)
		;

	template ProductionRule
		:	name
			(isDefined(parameters) ?
				"[" parameters{separator = ","} "]"
			)
			(isDefined(returns) ?
				"returns" "["
					"Object" "ret2"
				"]"
			)
			"@init" "{"
				-- if not needReturnDeclaration, then it is a label, which is automatically created by ANTLRv3, or it is not needed for some other reason
				(isDefined(returns) and needReturnDeclaration ?
					returns
				)
				(isDefined(declarations) ?
					declarations
				)
			"}"
			<newline> <tab> ":"
				(isDefined(expression) ?
					<tab> expression
				)
					[ "{" [
				(isDefined(actions) ?
						actions
				)
				(isDefined(returns) ?
						"ret2" "=" "ret" ";"
					)
					] "}" ] {indentIncr = 2}
			<newline> <tab> ";"
		;

	template VariableDeclaration
		:	type name
			(isDefined(initialValue) ?
				"=" initialValue
			)
			";"
		;

	template Parameter
		:	type name
		;

	template Variable
		:	declaration{refersTo = name}
		;

	template Expression abstract;

	template Alternative
		:	(isDefined(prevAction) ?
				"{" prevAction "}"
			)
			(one(expressions) ?
					expressions
				:
			"("
				(isDefined(options) ?
					"options" "{"
						options
					"}" ":"
				)
				expressions{separator = "|"}
			")"
			)
		;

	template Concatenation
		:	(isDefined(syntacticPredicate) ?
				"(" syntacticPredicate ")" "=>"
			)
			"("
				(isDefined(options) ?
					"options" "{"
						options
					"}" ":"
				)
				expressions
			")"
			(isDefined(action) ?
				"{" action "}"
			)
		;

	template Terminal
		:	"\'" <no_space> 'value' <no_space> "\'"
			(isDefined(action) ?
				"{" action "}"
			)
		;

-- @begin ForLexer
	template CharTerminal
		:	"\'" <no_space> value <no_space> "\'"
		;

	template Negation
		:	"~" expression
		;

	template Drop
		:	expression "!"
		;

	template Interval
		:	start ".." end
		;
-- @end ForLexer

	template RuleCall
		:	(isDefined(prevAction) ?
				"{" prevAction "}"
			)
			(isDefined(syntacticPredicate) ?
--				"(" syntacticPredicate ")" "=>"
			)
			(isDefined(storeTo) ?
				storeTo "="
			)
			calledRule{refersTo = name}
			(isDefined(arguments) ?
				"[" arguments{separator = ","} "]"
			)
			(isDefined(action) ?
				"{" action "}"
			)
		;

	template TokenCall
		:	(isDefined(prevAction) ?
				"{" prevAction "}"
			)
			(isDefined(storeASTTo) ?
				storeASTTo "="
			)
			name
			(isDefined(action) ?
				"{" action "}"
			)
		;

	template SemanticAction abstract;

	template SimpleSemanticAction
		:	'value'
		;

	template SemanticActionBlock
		:	actions
		;

	template ActionExpression abstract;

	template TextualExpression
		:	'value'
		;

	template LiteralExpression
		:	'value'
		;

	template Sequence_
		:	(isDefined(prevAction) ?
				"{" prevAction "}"
			)
			"("
				(isDefined(options) ?
					"options" "{"
						options
					"}" ":"
				)
				expression
			")"
			(lower = 0 ?
				(upper = 1 ? "?")
				(upper = -1 ? "*")
			)
			(lower = 1 ?
				(upper = -1 ? "+")
			)
			(isDefined(action) ?
				"{" action "}"
			)
			
		;

	symbols {
		lsquare		= "[";
		rsquare		= "]"	: rightSpace;
		excl		= "!";
		coma		= ",";
		lparen		= "(";
		rparen		= ")";
		lcurly		= "{"	: leftSpace;
		rcurly		= "}";
		semi		= ";";
		colon		= ":"	: leftSpace, rightSpace;
		colons		= "::";
		pipe		= "|";
		sharp		= "#";
		qmark		= "?";

		-- operator symbols
		point		= ".";
		rarrow		= "->";
		minus		= "-";
		star		= "*";
		slash		= "/";
		plus		= "+";
		eq		= "=";
		gt		= ">";
		lt		= "<";
		ge		= ">=";
		le		= "<=";
		ne		= "<>";
		larrow		= "<-";
	}

	lexer = "
		// no need for lexer for a serialization-only TCS model
	";

}

module TCS2Editor;
create OUT : Editor from IN : TCS;

helper context TCS!ConcreteSyntax def: getToken(name : String) : TCS!Token =
	self.tokens->any(e | e.name = name);

rule TCS2Editor {
	from
		s : TCS!ConcreteSyntax
	to
		t : Editor!Editor(
			extension <- 'mgm', -- not necessary
			group <- Set {keywords, symbols, types},
			block <- let ct : TCS!Token = s.getToken('COMMENT') in
				if ct.oclIsUndefined() then
					Sequence {}
				else
					ct.pattern.simplePatterns->collect(e | thisModule.createCommentBlock(e.rule))
				endif->including(
					let st : TCS!Token = s.getToken('STRING') in
					if st.oclIsUndefined() then
						thisModule.createStringBlock('\"', '\"', '\\')
					else
						let r : TCS!Rule = st.pattern.simplePatterns->first().rule in
						thisModule.createStringBlock(
							r.start.name,
							r.end.name,
							if r.esc.oclIsUndefined() then
								OclUndefined
							else
								r.esc.name
							endif
						)
					endif
				)
		),
		keywords : Editor!GroupElement(
			type <- 'keywords',
			format <- thisModule.createFormat('times', true, false, 127, 0, 85),
			element <- TCS!Keyword.allInstances()
		),
		symbols : Editor!GroupElement(
			type <- 'symbols',
			format <- thisModule.createFormat('times', true, false, 127, 0, 85),
			element <- TCS!Symbol.allInstances()
		),
		types : Editor!GroupElement(
			type <- 'types',
			format <- thisModule.createFormat('times', true, false, 42, 0, 255),
			element <- s.templates
		)
}

-- Create a block for comment syntax
rule createCommentBlock(r : TCS!RulePattern) {
	to
		t : Editor!Block(
			type <- 'CommentsLine',
			format <- thisModule.createFormat('times', false, true, 0, 130, 0)
		)
	do {
		t.blockbegin <- r.start.name;
		t.blockend <- if r.oclIsKindOf(TCS!MultiLineRule) then r.end.name else '' endif;
		t;
	}
}

-- Create a block for string syntax
rule createStringBlock(start : String, end : String, esc : String) {
	to
		t : Editor!Block(
			type <- 'string',
			blockbegin <- start,
			blockend <- end,
			esc <- esc,
			format <- thisModule.createFormat('times', false, false, 0, 0, 255)
		)
	do {
		t;
	}
}

rule createFormat(f : String, bold : Boolean, i : Boolean, r : Integer, g : Integer, b : Integer) {
	to
		t : Editor!Format(
			color <- thisModule.createColor(r, g, b),
			font <- thisModule.createFont(f, bold, i)
		)
	do {
		t;
	}
}

rule createFont(f : String, b : Boolean, i : Boolean) {
	to
		t : Editor!Font(
			font <- f,
			bold <- b,
			italic <- i
		)
	do {
		t;
	}
}

rule createColor(r : Integer, g : Integer, b : Integer) {
	to
		t : Editor!Color(
			red <- r,
			green <- g,
			blue <- b
		)
	do {
		t;
	}
}

rule keyword {
	from
		s : TCS!Keyword
	to
		t : Editor!Element(
			element <- s.value
		)
}

rule symbol {
	from
		s : TCS!Symbol
	to
		t : Editor!Element(
			element <- s.value
		)
}

rule type {
	from
		s : TCS!PrimitiveTemplate
	to
		t : Editor!Element(
			element <- s.typeName
		)
}

--	groupElement 'constants'{
--		indent '0';
--		format {
--			color 24, 159, 71;
--			font 'times', false, false;
--		}
--		element 'true';
--	}

/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $$Id: InjectAction.java,v 1.3 2008/04/08 15:11:57 fjouault Exp $$
 */
package org.eclipse.gmt.tcs.actions;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.gmt.tcs.metadata.ASMModelFactory;
import org.eclipse.gmt.tcs.metadata.Language;
import org.eclipse.gmt.tcs.metadata.LanguageRegistry;
import org.eclipse.gmt.tcs.metadata.Utils;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.m2m.atl.dsls.textsource.IFileTextSource;
import org.eclipse.m2m.atl.engine.vm.AtlModelHandler;
import org.eclipse.m2m.atl.engine.vm.ModelLoader;
import org.eclipse.m2m.atl.engine.vm.nativelib.ASMModel;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

/**
 * 
 * @author Fr�d�ric Jouault
 *
 */
public class InjectAction implements IObjectActionDelegate {

	private IStructuredSelection selection;

	public InjectAction() {
		// TODO Auto-generated constructor stub
	}

	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// TODO Auto-generated method stub

	}

	public void run(IAction action) {
		IFile file = getSelectedElement();
		Language language = LanguageRegistry.getDefault().getLanguage(getSelectedElement().getFileExtension());
		ModelLoader ml = AtlModelHandler.getDefault(AtlModelHandler.AMH_EMF).createModelLoader();
		Map params = new HashMap();

		// TODO: pass a problem model in order for the WFR (sometimes also acting as external resolver) to be executed
		ASMModel model = (ASMModel)language.inject(ASMModelFactory.getDefault(), null, new IFileTextSource(file), params);
		try {
			Utils.save(ml, model, file.getProject().getFile(file.getProjectRelativePath().toString() + ".xmi"));
		} catch (IOException e) {
			throw new RuntimeException("saving injected model failed", e);
		}
	}

	public void selectionChanged(IAction action, ISelection selection) {
		this.selection = (IStructuredSelection)selection;
		IFile file = getSelectedElement();
		if(file != null) {
			Language language = LanguageRegistry.getDefault().getLanguage(file.getFileExtension());
			if(language != null) {
				action.setText("Inject " + language.getName() + " file to " + language.getName() + " model");
				action.setEnabled(true);
			} else {
				action.setEnabled(false);
				action.setText("Cannot inject");
			}
		} else {
			action.setEnabled(false);
		}
	}

	private IFile getSelectedElement() {
		return (IFile)selection.getFirstElement();
	}
}

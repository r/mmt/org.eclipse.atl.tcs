/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: StartUp.java,v 1.1 2008/04/02 02:08:29 fjouault Exp $
 */
package org.eclipse.gmt.tcs.actions;

import org.eclipse.ui.IStartup;

/**
 * @author Fr�d�ric Jouault
 *
 */
public class StartUp implements IStartup {

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IStartup#earlyStartup()
	 */
	public void earlyStartup() {
		// We only need startup to make sure this plugin
		// is activated, so that the actions can disable themselves.
	}

}

/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $$Id: ExtractAction.java,v 1.3 2008/04/08 15:11:57 fjouault Exp $$
 */
package org.eclipse.gmt.tcs.actions;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.gmt.tcs.metadata.ASMModelFactory;
import org.eclipse.gmt.tcs.metadata.Language;
import org.eclipse.gmt.tcs.metadata.LanguageRegistry;
import org.eclipse.gmt.tcs.metadata.ModelFactory;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.m2m.atl.drivers.emf4atl.ASMEMFModelElement;
import org.eclipse.m2m.atl.drivers.emf4atl.EMFModelLoader;
import org.eclipse.m2m.atl.engine.vm.AtlModelHandler;
import org.eclipse.m2m.atl.engine.vm.ModelLoader;
import org.eclipse.m2m.atl.engine.vm.nativelib.ASMModel;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

/**
 *
 * @author Fr�d�ric Jouault
 *
 */
public class ExtractAction implements IObjectActionDelegate {

	private IStructuredSelection selection;

	public ExtractAction() {
		// TODO Auto-generated constructor stub
	}

	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// TODO Auto-generated method stub

	}

	public void run(IAction action) {
		IFile file = getSelectedElement();
		Language language = getLanguage();
		ModelLoader ml = AtlModelHandler.getDefault(AtlModelHandler.AMH_EMF).createModelLoader();
		ModelFactory factory = ASMModelFactory.getDefault();
		ASMModel metamodel = (ASMModel)language.getMetamodel(factory);
		try {
			BufferedInputStream in = new BufferedInputStream(file.getContents());

			// Register metamodel in newly created model loader (TODO: how could we avoid doing this here? maybe directly using EMF will make this unnecessary)
			// Alternatively, the same model loader as the one used for the metamodel may be used.
			ResourceSet resourceSet = ((EMFModelLoader)ml).getResourceSet();
			for (Iterator i = metamodel.getElementsByType("EPackage").iterator(); i.hasNext();) {
				ASMEMFModelElement ame = (ASMEMFModelElement)i.next();
				EPackage p = (EPackage)ame.getObject();
				String nsURI = p.getNsURI();
				synchronized (resourceSet) {
					resourceSet.getPackageRegistry().put(nsURI, p);
				}
			}

			ASMModel model = ml.loadModel("model.xmi", metamodel, in);
			in.close();
			Map params = new HashMap();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			language.extract(factory, model, out, params);
			IFile outFile = file.getParent().getFile(new Path(file.getName() + "." + language.getExtension()));
			if(outFile.exists()) {
				outFile.setContents(new ByteArrayInputStream(out.toByteArray()), IFile.FORCE + IFile.DERIVED, null);
			} else {
				outFile.create(new ByteArrayInputStream(out.toByteArray()), IFile.FORCE + IFile.DERIVED, null);
			}
		} catch(IOException e) {
			throw new RuntimeException("Error extracting " + file.toString(), e);
		} catch (CoreException e) {
			throw new RuntimeException("Error extracting " + file.toString(), e);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void selectionChanged(IAction action, ISelection selection) {
		this.selection = (IStructuredSelection)selection;
		Language language = getLanguage();
		if(language != null) {
			action.setText("Extract " + language.getName() + " model to " + language.getName() + " file (." + language.getExtension() + ")");
			action.setEnabled(true);
		} else {
			action.setEnabled(false);
			action.setText("Cannot extract");
		}
	}

	private Language getLanguage() {
		IFile file = getSelectedElement();
		if(file != null) {
			String fileNameParts[] = getSelectedElement().getName().split("\\.");
			if(fileNameParts.length > 2) {
				int last = fileNameParts.length - 1;
				if("ecore".equals(fileNameParts[last]) || "xmi".equals(fileNameParts[last])) {
					String formatExtension = fileNameParts[last - 1];  
					Language language = LanguageRegistry.getDefault().getLanguage(formatExtension);
					return language;
				}
			}
		}
		return null;
	}
	private IFile getSelectedElement() {
		return (IFile)selection.getFirstElement();
	}
}

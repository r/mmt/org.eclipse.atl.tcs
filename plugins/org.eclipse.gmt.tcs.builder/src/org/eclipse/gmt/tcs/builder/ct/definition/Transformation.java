/**
 * Copyright (c) 2007 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: Transformation.java,v 1.2 2008/06/25 13:01:57 fjouault Exp $
 */
package org.eclipse.gmt.tcs.builder.ct.definition;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.gmt.tcs.builder.ct.usage.TransformationUsage;

/**
 * 
 * @author Fr�d�ric Jouault
 *
 */
public abstract class Transformation {

	protected Map options = new HashMap();
	public List sources = new ArrayList();
	public List targets = new ArrayList();
	
	public void addSource(SourceDefinition source) {
		sources.add(source);
	}

	public void addTarget(TargetDefinition target) {
		targets.add(target);
	}

	public abstract void execute(TransformationUsage usage, Map models) throws IOException;

	public void setOption(String name, String value) {
		options.put(name, value);
	}
}

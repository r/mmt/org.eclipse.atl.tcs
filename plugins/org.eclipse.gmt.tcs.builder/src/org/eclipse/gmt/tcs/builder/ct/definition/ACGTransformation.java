/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: ACGTransformation.java,v 1.2 2008/06/25 13:01:57 fjouault Exp $
 */
package org.eclipse.gmt.tcs.builder.ct.definition;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.gmt.tcs.builder.ct.usage.Source;
import org.eclipse.gmt.tcs.builder.ct.usage.Target;
import org.eclipse.gmt.tcs.builder.ct.usage.TransformationUsage;
import org.eclipse.m2m.atl.dsls.textsource.ByteArrayTextSource;
import org.eclipse.m2m.atl.dsls.textsource.TextSource;
import org.eclipse.m2m.atl.engine.ASMRetriever;
import org.eclipse.m2m.atl.engine.AtlLauncher;
import org.eclipse.m2m.atl.engine.AtlModelHandler;
import org.eclipse.m2m.atl.engine.vm.ASM;
import org.eclipse.m2m.atl.engine.vm.ASMXMLReader;
import org.eclipse.m2m.atl.engine.vm.ASMXMLWriter;
import org.eclipse.m2m.atl.engine.vm.Debugger;
import org.eclipse.m2m.atl.engine.vm.SimpleDebugger;
import org.eclipse.m2m.atl.engine.vm.nativelib.ASMModel;

/**
 * 
 * @author Fr�d�ric Jouault
 *
 */
public class ACGTransformation extends Transformation {

	public void execute(TransformationUsage usage, Map contextualModels) throws IOException {
		Map libraries = new HashMap(this.libraries);
		AtlModelHandler amh = AtlModelHandler.getDefault(AtlModelHandler.AMH_EMF);

		Map models = new HashMap();
		models.put("MOF", amh.getMof());
		for(Iterator i = usage.sources.iterator() ; i.hasNext() ; ) {
			Source source = (Source)i.next();
			ATLSourceDefinition sourceDef = (ATLSourceDefinition)source.definition;
			ASMModel sourceModel = (ASMModel)source.origin.get(contextualModels);
			ASMModel sourceMetamodel = sourceDef.getMetamodel().getMetamodel();
			sourceModel.setIsTarget(false);
			models.put(sourceDef.getMetamodelName(), sourceMetamodel);
			models.put(sourceDef.getModelName(), sourceModel);
		}
/*		for(Iterator i = usage.targets.iterator() ; i.hasNext() ; ) {
			Target target = (Target)i.next();
			ATLTargetDefinition targetDef = (ATLTargetDefinition)target.definition;
			ASMModel targetMetamodel = targetDef.getMetamodel().getMetamodel();
			models.put(targetDef.getMetamodelName(), targetMetamodel);
			ASMModel targetModel = amh.newModel(targetDef.getModelName(), "myTarget.xmi", targetMetamodel);
			models.put(targetDef.getModelName(), targetModel);

			contextualModels.put(target, targetModel);
		}
*/
		IFile file = ASMRetriever.getFile();
		List superimpose = Collections.EMPTY_LIST;
		
		Map options = Collections.EMPTY_MAP;
		
		Debugger debugger = new SimpleDebugger(
				/* step = */ "true".equals(options.get("step")),
				/* stepops = */ new ArrayList(),
				/* deepstepops = */ new ArrayList(),
				/* nostepops = */ new ArrayList(),
				/* deepnostepops = */ new ArrayList(),
				/* showStackTrace = */ true,
				"true".equals(options.get("showSummary")),
				"true".equals(options.get("profile")),
				"true".equals(options.get("continueAfterError"))
		);
//		debugger = new NetworkDebugger(6060, true);

		Map asmParams = new HashMap();
        asmParams.put("debug", "false");//$NON-NLS-1$//$NON-NLS-2$
        asmParams.put("WriteTo", file.getLocation().toString());//$NON-NLS-1$
		AtlLauncher.getDefault().launch(asm, libraries, models, asmParams, superimpose, options, debugger);
		ASM asm = ASMRetriever.getASM(file);
		Target target = (Target)usage.targets.get(0);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Map params = new HashMap();
		params.putAll(options);
		PrintWriter pw = new PrintWriter(baos);
		new ASMXMLWriter(pw, false).print(asm);
		pw.close();
		contextualModels.put(target, new ByteArrayTextSource(baos.toByteArray()));
	}

	private ASM asm;
	public Map libraries;
	
	public ACGTransformation(TextSource transformationTS) throws IOException {
		this(transformationTS, Collections.EMPTY_MAP);
	}
	
	public ACGTransformation(TextSource transformationTS, Map libraries) throws IOException {
		asm = new ASMXMLReader().read(transformationTS.openStream());
		
		this.libraries = new HashMap();
		for(Iterator i = libraries.keySet().iterator() ; i.hasNext() ; ) {
			Object key = i.next();
			addLibrary(key, (TextSource)libraries.get(key));
		}
	}
	
	public void addLibrary(Object name, TextSource ts) throws IOException {
		this.libraries.put(name, new ASMXMLReader().read(ts.openStream()));		
	}
	
	public ASM getASM() {
		return asm;
	}
}

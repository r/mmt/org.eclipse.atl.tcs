/**
 * Copyright (c) 2007 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: CompositeTransformation.java,v 1.2 2008/06/25 13:01:57 fjouault Exp $
 */
package org.eclipse.gmt.tcs.builder.ct;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.gmt.tcs.builder.ct.definition.CompositeTransformationTargetDefinition;
import org.eclipse.gmt.tcs.builder.ct.definition.Transformation;
import org.eclipse.gmt.tcs.builder.ct.usage.Target;
import org.eclipse.gmt.tcs.builder.ct.usage.TransformationUsage;

/**
 * 
 * @author Fr�d�ric Jouault
 *
 */
public class CompositeTransformation extends Transformation {

	public Object getTarget(CompositeTransformationTargetDefinition target, Map models) throws IOException {
		return target.get(models);
	}

	/**
	 * Populates the models Map with all targets. Note that each target may also be retrieved separately
	 * by directly calling its get method.
	 * @throws IOException 
	 */
	public void execute(TransformationUsage usage, Map models) throws IOException {
		for(Iterator i = usage.targets.iterator() ; i.hasNext() ; ) {
			Target target = (Target)i.next();
			models.put(target, target.get(models));
		}
	}
}

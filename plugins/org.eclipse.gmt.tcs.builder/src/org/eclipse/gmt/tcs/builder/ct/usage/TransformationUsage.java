/**
 * Copyright (c) 2007, 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: TransformationUsage.java,v 1.3 2008/06/25 13:01:57 fjouault Exp $
 */
package org.eclipse.gmt.tcs.builder.ct.usage;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gmt.tcs.builder.ct.definition.Transformation;

/**
 * 
 * @author Fr�d�ric Jouault
 *
 */
public class TransformationUsage {

//	private Transformation transformation;
	public List sources = new ArrayList();
	public List targets = new ArrayList();
	
	public TransformationUsage(Transformation transformation) {
//		this.transformation = transformation;
	}

}

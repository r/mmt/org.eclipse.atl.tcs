/**
 * Copyright (c) 2007 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: CompositeTransformationSourceDefinition.java,v 1.2 2008/06/25 13:01:57 fjouault Exp $
 */
package org.eclipse.gmt.tcs.builder.ct.definition;

import java.util.Map;

import org.eclipse.gmt.tcs.builder.ct.CompositeTransformation;
import org.eclipse.gmt.tcs.builder.ct.Metamodel;
import org.eclipse.gmt.tcs.builder.ct.Origin;

/**
 * 
 * @author Fr�d�ric Jouault
 *
 */
public class CompositeTransformationSourceDefinition extends ModelSourceDefinition implements Origin {

	public CompositeTransformationSourceDefinition(CompositeTransformation transformation, Metamodel metamodel) {
		super(transformation, metamodel);
	}

	public Object get(Map models) {
		Object ret = models.get(this);
		
		if(ret == null)
			throw new RuntimeException("source of composite transformation missing");
		
		return ret;
	}

}

/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: ATLRefinedTargetDefinition.java,v 1.2 2008/06/25 13:01:56 fjouault Exp $
 */
package org.eclipse.gmt.tcs.builder.ct.definition;

/**
 * 
 * @author Fr�d�ric Jouault
 *
 */
public class ATLRefinedTargetDefinition extends ModelTargetDefinition {

	private ATLSourceDefinition source;

	public ATLRefinedTargetDefinition(Transformation transformation, SourceDefinition source) {
		super(transformation, ((ATLSourceDefinition)source).getMetamodel());
		this.source = (ATLSourceDefinition)source;
	}

	public ATLSourceDefinition getSourceDefinition() {
		return source;
	}
}

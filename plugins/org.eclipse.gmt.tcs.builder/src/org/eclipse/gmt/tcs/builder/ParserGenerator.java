/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: ParserGenerator.java,v 1.1 2008/06/28 17:15:27 fjouault Exp $
 */
package org.eclipse.gmt.tcs.builder;

/**
 * 
 * @author Fr�d�ric Jouault
 * 
 */
public interface ParserGenerator {

	public void generate(String antlrFile, String javaFilesPath, ParserGeneratorErrorListener errorListener);
}

/**
 * Copyright (c) 2007, 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: Source.java,v 1.3 2008/06/25 13:01:57 fjouault Exp $
 */
package org.eclipse.gmt.tcs.builder.ct.usage;

import org.eclipse.gmt.tcs.builder.ct.Origin;
import org.eclipse.gmt.tcs.builder.ct.definition.SourceDefinition;

/**
 * 
 * @author Fr�d�ric Jouault
 *
 */
public class Source {

//	private TransformationUsage usage;
	public SourceDefinition definition;
	public Origin origin;
	
	public Source(TransformationUsage usage, SourceDefinition definition, Origin origin) {
//		this.usage = usage;
		this.definition = definition;
		this.origin = origin;
		
		usage.sources.add(this);
	}
}

/**
 * Copyright (c) 2007, 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: Target.java,v 1.3 2008/06/25 13:01:57 fjouault Exp $
 */
package org.eclipse.gmt.tcs.builder.ct.usage;

import java.io.IOException;
import java.util.Map;

import org.eclipse.gmt.tcs.builder.ct.Origin;
import org.eclipse.gmt.tcs.builder.ct.definition.TargetDefinition;

/**
 * 
 * @author Fr�d�ric Jouault
 *
 */
public class Target implements Origin {

	private TransformationUsage usage;
	public TargetDefinition definition;
	
	public Target(TransformationUsage usage, TargetDefinition definition) {
		this.usage = usage;
		this.definition = definition;
		
		usage.targets.add(this);
	}

	public Object get(Map models) throws IOException {
		Object ret = models.get(this);
		
		if(ret == null) {
			definition.transformation.execute(usage, models);
		}

		return models.get(this);
	}
}

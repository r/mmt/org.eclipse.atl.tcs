/**
 * Copyright (c) 2007 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: CompositeTransformationTargetDefinition.java,v 1.2 2008/06/25 13:01:57 fjouault Exp $
 */
package org.eclipse.gmt.tcs.builder.ct.definition;

import java.io.IOException;
import java.util.Map;

import org.eclipse.gmt.tcs.builder.ct.Metamodel;
import org.eclipse.gmt.tcs.builder.ct.usage.Target;

/**
 * 
 * @author Fr�d�ric Jouault
 *
 */
public class CompositeTransformationTargetDefinition extends ModelTargetDefinition {

	private Target source;
	
	public CompositeTransformationTargetDefinition(Transformation transformation, Metamodel metamodel) {
		super(transformation, metamodel);
	}

	public void setSource(Target source) {
		this.source = source;
	}

	public Object get(Map models) throws IOException {
		return source.get(models);
	}

}

/**
 * Copyright (c) 2007, 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: ProblemReporter.java,v 1.3 2008/06/25 13:03:20 fjouault Exp $
 */
package org.eclipse.gmt.tcs.builder;

import org.eclipse.m2m.atl.dsls.textsource.TextSource;
import org.eclipse.m2m.atl.engine.vm.nativelib.ASMModel;

/**
 * 
 * @author Fr�d�ric Jouault
 * 
 */
public interface ProblemReporter {

	public boolean reportProblem(TextSource textSource, ASMModel problemModel);
}

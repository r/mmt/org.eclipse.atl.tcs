/**
 * Copyright (c) 2007, 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: Metamodel.java,v 1.5 2008/06/25 13:01:57 fjouault Exp $
 */
package org.eclipse.gmt.tcs.builder.ct;

import java.io.IOException;

import org.eclipse.gmt.tcs.builder.ct.definition.ATLTransformation;
import org.eclipse.m2m.atl.dsls.Resource;
import org.eclipse.m2m.atl.dsls.textsource.TextSource;
import org.eclipse.m2m.atl.engine.AtlEMFModelHandler;
import org.eclipse.m2m.atl.engine.AtlModelHandler;
import org.eclipse.m2m.atl.engine.vm.nativelib.ASMModel;

/**
 * 
 * @author Fr�d�ric Jouault
 *
 */
public class Metamodel {

	public String name; 
	public ASMModel metamodel;
	public ATLTransformation checker = null;
	public TextSource source = null;

	public Metamodel(String name, String nsURI) throws IOException {
		this.name = name;
		
		AtlEMFModelHandler amh = (AtlEMFModelHandler)AtlModelHandler.getDefault(AtlModelHandler.AMH_EMF);
		
		metamodel = amh.loadModel(name, amh.getMof(), "uri:" + nsURI);
	}
	
	public Metamodel(String name, ASMModel metamodel) throws IOException {
		this.name = name;
		this.metamodel = metamodel;
	}
	
	public Metamodel(String name, TextSource metamodelTS) throws IOException {
		this.name = name;
		
		AtlModelHandler amh = AtlModelHandler.getDefault(AtlModelHandler.AMH_EMF);

		metamodel = amh.loadModel(name, amh.getMof(), metamodelTS.openStream());
		source = metamodelTS;
	}

	public Metamodel(String name, Resource metamodelResource) throws IOException {
		this.name = name;

		AtlEMFModelHandler amh = (AtlEMFModelHandler)AtlModelHandler.getDefault(AtlModelHandler.AMH_EMF);

		metamodel = amh.loadModel(name, amh.getMof(), metamodelResource.asEMFURI());
		source = metamodelResource.asTextSource();
	}

	public String getName() {
		return name;
	}
	
	public ASMModel getMetamodel() {
		return metamodel;
	}
}

/**
 * Copyright (c) 2007, 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: TCSInjection.java,v 1.6 2008/06/25 13:01:56 fjouault Exp $
 */
package org.eclipse.gmt.tcs.builder.ct.definition;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.gmt.tcs.builder.ProblemReporter;
import org.eclipse.gmt.tcs.builder.ct.Metamodel;
import org.eclipse.gmt.tcs.builder.ct.usage.Source;
import org.eclipse.gmt.tcs.builder.ct.usage.Target;
import org.eclipse.gmt.tcs.builder.ct.usage.TransformationUsage;
import org.eclipse.gmt.tcs.injector.TCSInjector;
import org.eclipse.m2m.atl.dsls.textsource.TextSource;
import org.eclipse.m2m.atl.engine.AtlModelHandler;
import org.eclipse.m2m.atl.engine.injectors.Injector;
import org.eclipse.m2m.atl.engine.vm.nativelib.ASMModel;

/**
 * 
 * @author Fr�d�ric Jouault
 *
 */
public class TCSInjection extends Transformation {

	public void execute(TransformationUsage usage, Map models) throws IOException {
		AtlModelHandler amh = AtlModelHandler.getDefault(AtlModelHandler.AMH_EMF);

		Target target = null;
		ModelTargetDefinition targetDef = null;
		Target problem = null;
		ModelTargetDefinition problemDef = null;
		for(Iterator i = usage.targets.iterator() ; i.hasNext() ; ) {
			Target tgt = (Target)i.next();
			ModelTargetDefinition tgtDef = (ModelTargetDefinition)tgt.definition;
			if("Problem".equals(tgtDef.getMetamodel().name)) {
				problem = tgt;
				problemDef = tgtDef;
			} else {
				target = tgt;
				targetDef = tgtDef;
			}
		}


		Injector inj = new TCSInjector();
		Map params = new HashMap();
		String name = targetDef.getMetamodel().getName();
		params.put("name", name);
		params.put("parserGenerator", parserGenerator);
		try {
			params.put("lexerClass", getClass().getClassLoader().loadClass("org.eclipse.gmt.tcs.injector." + name + "_ANTLR3Lexer"));
			params.put("parserClass", getClass().getClassLoader().loadClass("org.eclipse.gmt.tcs.injector." + name + "_ANTLR3Parser"));
		} catch (ClassNotFoundException e) {
			IOException ioe = new IOException("problem loading lexer and/or parser class");
			ioe.initCause(e);
			throw ioe;
		}
		params.put("tabSize", "1");
		ASMModel problemModel = null;
		ProblemReporter problemReporter = null;
		if(problemMetamodel != null)
			problemReporter = (ProblemReporter)models.get("problemReporter");
		if(problemDef != null) {
			problemModel = amh.newModel("pbs", "pbs.xmi", problemDef.getMetamodel().getMetamodel());
			params.put("problems", problemModel);
		} else if(problemReporter != null) {
			problemModel = amh.newModel("pbs", "pbs.xmi", problemMetamodel.getMetamodel());
			params.put("problems", problemModel);
		}
		ASMModel targetModel = amh.newModel("myTarget", "myTarget.xmi", targetDef.getMetamodel().getMetamodel());
		TextSource textSource = (TextSource)((Source)usage.sources.get(0)).origin.get(models);
		InputStream stream = textSource.openStream();
		inj.inject(targetModel, stream, params);
		stream.close();
		models.put(target, targetModel);
		if(problemDef != null) {
			models.put(problem, problemModel);
		} else if(problemReporter != null) {
			if(!problemReporter.reportProblem(textSource, problemModel)) {
				models.remove(target); 
			}
		}
	}

	private String parserGenerator;
	private Metamodel problemMetamodel;
	
	public TCSInjection() throws IOException {
		this(null);
	}
	
	public TCSInjection(Metamodel problemMetamodel) throws IOException {
		this(problemMetamodel, "antlr3");
	}
	
	public TCSInjection(Metamodel problemMetamodel, String parserGenerator) throws IOException {
		this.parserGenerator = parserGenerator;
		this.problemMetamodel = problemMetamodel;
	}
}

/**
 * Copyright (c) 2007, 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: TCSBuilderCT.java,v 1.7 2008/06/25 13:02:47 fjouault Exp $
 */
package org.eclipse.gmt.tcs.builder;

import java.io.IOException;
import java.util.Map;

import org.eclipse.gmt.tcs.builder.ct.CompositeTransformation;
import org.eclipse.gmt.tcs.builder.ct.Metamodel;
import org.eclipse.gmt.tcs.builder.ct.ResourceLocator;
import org.eclipse.gmt.tcs.builder.ct.definition.ACGTransformation;
import org.eclipse.gmt.tcs.builder.ct.definition.ATLRefinedTargetDefinition;
import org.eclipse.gmt.tcs.builder.ct.definition.ATLSourceDefinition;
import org.eclipse.gmt.tcs.builder.ct.definition.ATLTargetDefinition;
import org.eclipse.gmt.tcs.builder.ct.definition.ATLTransformation;
import org.eclipse.gmt.tcs.builder.ct.definition.CompositeTransformationSourceDefinition;
import org.eclipse.gmt.tcs.builder.ct.definition.CompositeTransformationTargetDefinition;
import org.eclipse.gmt.tcs.builder.ct.definition.ModelSourceDefinition;
import org.eclipse.gmt.tcs.builder.ct.definition.ModelTargetDefinition;
import org.eclipse.gmt.tcs.builder.ct.definition.SourceDefinition;
import org.eclipse.gmt.tcs.builder.ct.definition.TCSExtraction;
import org.eclipse.gmt.tcs.builder.ct.definition.TCSInjection;
import org.eclipse.gmt.tcs.builder.ct.definition.TargetDefinition;
import org.eclipse.gmt.tcs.builder.ct.definition.Transformation;
import org.eclipse.gmt.tcs.builder.ct.usage.Source;
import org.eclipse.gmt.tcs.builder.ct.usage.Target;
import org.eclipse.gmt.tcs.builder.ct.usage.TransformationUsage;
import org.eclipse.m2m.atl.engine.AtlModelHandler;

/**
 * 
 * @author Fr�d�ric Jouault
 *
 */
public class TCSBuilderCT {
	
	public Object getTarget(CompositeTransformationTargetDefinition target, Map models) throws IOException {
		return ct.getTarget(target, models);
	}

	public CompositeTransformation ct;
	public CompositeTransformationSourceDefinition srckm3;
	public CompositeTransformationSourceDefinition srcannotation;
	public CompositeTransformationSourceDefinition srctcs;
	public CompositeTransformationSourceDefinition srcacg;
	public CompositeTransformationTargetDefinition tgttcsproblem;
//	public CompositeTransformationTargetDefinition tgttcssyntaxproblem;
	public CompositeTransformationTargetDefinition tgtkm3problem;
	public CompositeTransformationTargetDefinition tgtacgproblem;
	public CompositeTransformationTargetDefinition tgtasm;
	public CompositeTransformationTargetDefinition tgttcs;
//	public CompositeTransformationTargetDefinition tgtkm3syntaxproblem;
	public CompositeTransformationTargetDefinition tgteditor;
	public CompositeTransformationTargetDefinition tgtoutline;
	public CompositeTransformationTargetDefinition tgtgrammar;
	public CompositeTransformationTargetDefinition tgtecore;
	public CompositeTransformationTargetDefinition tgtecorePlusAnnotations;

	public TCSBuilderCT(ResourceLocator rl) throws IOException {
		AtlModelHandler amh = AtlModelHandler.getDefault(AtlModelHandler.AMH_EMF);

		this.rl = rl;
		Metamodel km3mm = getMetamodel("KM3", "-2005");
		Metamodel tcsmm = getMetamodel("TCS");
		Metamodel annotationmm = getMetamodel("Annotation");
		Metamodel refiningTracemm = new Metamodel("RefiningTrace", rl.getResource("ATL/RuntimeSupport/RefiningTrace.ecore"));

		Metamodel editormm = getMetamodel("Editor");
		Metamodel outlinemm = getMetamodel("Outline");

		Metamodel problemmm = getMetamodel("Problem");
		Metamodel antlrmm = getMetamodel("ANTLR");

		Metamodel acgmm = getMetamodel("ACG");

		Metamodel ecoremm = new Metamodel("Ecore", amh.getMof());
		
		// TCS Injection
		Transformation tcsinjection = new TCSInjection(problemmm);
		SourceDefinition tcsinjectionSrc = new SourceDefinition(tcsinjection);
		TargetDefinition tcsinjectionTgt = new ModelTargetDefinition(tcsinjection, tcsmm);
//		TargetDefinition tcsinjectionProblemTgt = new ModelTargetDefinition(tcsinjection, problemmm);
		
		// TCS2Problem
		ATLTransformation tcs2problem = new ATLTransformation(rl.getResource("TCS/WFR/TCS2Problem.asm").asTextSource());
		tcs2problem.addLibrary("KM3Helpers", rl.getResource("KM3-2005/Helpers/KM3Helpers.asm").asTextSource());
		SourceDefinition tcs2problemTCSSrc = new ATLSourceDefinition(tcs2problem, "IN", "TCS", tcsmm);
		SourceDefinition tcs2problemKM3Src = new ATLSourceDefinition(tcs2problem, "MM", "KM3", km3mm);
		TargetDefinition tcs2problemTgt = new ATLTargetDefinition(tcs2problem, "OUT", "Problem", problemmm);

		// TCS2Editor
		Transformation tcs2editor = new ATLTransformation(rl.getResource("Editor/Compiler/TCS2Editor.asm").asTextSource());
		SourceDefinition tcs2editorSrc = new ATLSourceDefinition(tcs2editor, "IN", "TCS", tcsmm);
		TargetDefinition tcs2editorTgt = new ATLTargetDefinition(tcs2editor, "OUT", "Editor", editormm);

		// KM3 Injection
		Transformation km3injection = new TCSInjection(problemmm);
		SourceDefinition km3injectionSrc = new SourceDefinition(km3injection);
		TargetDefinition km3injectionTgt = new ModelTargetDefinition(km3injection, km3mm);
//		TargetDefinition km3injectionProblemTgt = new ModelTargetDefinition(km3injection, problemmm);
		
		// KM32Problem
		ATLTransformation km32problem = new ATLTransformation(rl.getResource("KM3-2005/WFR/KM32Problem.asm").asTextSource());
		SourceDefinition km32problemSrc = new ATLSourceDefinition(km32problem, "IN", "KM3", km3mm);
		TargetDefinition km32problemTgt = new ATLTargetDefinition(km32problem, "OUT", "Problem", problemmm);

		// KM32Outline
		ATLTransformation km32outline = new ATLTransformation(rl.getResource("Outline/Compiler/KM32Outline.asm").asTextSource());
		km32outline.addLibrary("KM3Helpers", rl.getResource("KM3-2005/Helpers/KM3Helpers.asm").asTextSource());
		SourceDefinition km32outlineSrc = new ATLSourceDefinition(km32outline, "IN", "KM3", km3mm);
		TargetDefinition km32outlineTgt = new ATLTargetDefinition(km32outline, "OUT", "Outline", outlinemm);

		// ACG Injection
		Transformation acginjection = new TCSInjection(problemmm);
		SourceDefinition acginjectionSrc = new SourceDefinition(acginjection);
		TargetDefinition acginjectionTgt = new ModelTargetDefinition(acginjection, acgmm);
//		TargetDefinition acginjectionProblemTgt = new ModelTargetDefinition(acginjection, problemmm);
		
		// ACGCompilation
		ACGTransformation acgCompilation = new ACGTransformation(rl.getResource("ACG/Compiler/ACG.asm").asTextSource());
		SourceDefinition acgCompilationSrc = new ATLSourceDefinition(acgCompilation, "IN", "ACG", acgmm);
		TargetDefinition acgCompilationTgt = new TargetDefinition(acgCompilation);

		// KM32Ecore
		ATLTransformation km32ecore = new ATLTransformation(rl.getResource("KM3-2005/Compiler/KM32Ecore.asm").asTextSource());
//		km32ecore.addLibrary("KM3Helpers", rl.getResource("KM3-2005/Helpers/KM3Helpers.asm"));
		SourceDefinition km32ecoreSrc = new ATLSourceDefinition(km32ecore, "IN", "KM3", km3mm);
		TargetDefinition km32ecoreTgt = new ATLTargetDefinition(km32ecore, "OUT", "Ecore", ecoremm);

		// Annotation Injection
		Transformation annotationinjection = new TCSInjection(problemmm);
		SourceDefinition annotationinjectionSrc = new SourceDefinition(annotationinjection);
		TargetDefinition annotationinjectionTgt = new ModelTargetDefinition(annotationinjection, annotationmm);
//		TargetDefinition annotationinjectionProblemTgt = new ModelTargetDefinition(annotationinjection, problemmm);

		// ApplyAnnotations2Ecore
		ATLTransformation applyAnnotations2ecore = new ATLTransformation(rl.getResource("Annotation/Compiler/ApplyAnnotations2Ecore.asm").asTextSource());
		SourceDefinition applyAnnotations2ecoreSrc = new ATLSourceDefinition(applyAnnotations2ecore, "IN", "Ecore", ecoremm);
		SourceDefinition applyAnnotations2ecoreAnnotationSrc = new ATLSourceDefinition(applyAnnotations2ecore, "annotations", "Annotation", annotationmm);
		TargetDefinition applyAnnotations2ecoreTgt = new ATLRefinedTargetDefinition(applyAnnotations2ecore, applyAnnotations2ecoreSrc);
		TargetDefinition applyAnnotations2ecoreRefiningTraceTgt = new ATLTargetDefinition(applyAnnotations2ecore, "refiningTrace", "RefiningTrace", refiningTracemm);

		// TCS2ANTLR
		ATLTransformation tcs2antlr = new ATLTransformation(rl.getResource("TCS/Compiler/TCS2ANTLR.asm").asTextSource());
		tcs2antlr.addLibrary("TCS2ANTLRActions", rl.getResource("TCS/Compiler/TCS2ANTLRv3Actions.asm").asTextSource());
		tcs2antlr.addLibrary("KM3Helpers", rl.getResource("KM3-2005/Helpers/KM3Helpers.asm").asTextSource());
		tcs2antlr.addLibrary("strings", rl.getResource("libs/strings.asm").asTextSource());
		SourceDefinition tcs2antlrTCSSrc = new ATLSourceDefinition(tcs2antlr, "IN", "TCS", tcsmm);
		SourceDefinition tcs2antlrKM3Src = new ATLSourceDefinition(tcs2antlr, "MM", "KM3", km3mm);
		TargetDefinition tcs2antlrTgt = new ATLTargetDefinition(tcs2antlr, "OUT", "ANTLR", antlrmm);

		// ANTLR Extraction
									// not asEMFURI() because then this model is somehow not disposed off before its metamodel is
		Transformation antlrextraction = new TCSExtraction(tcsmm, rl.getResource("ANTLR/Syntax/ANTLR-TCS.xmi").asURL());
		SourceDefinition antlrextractionSrc = new ModelSourceDefinition(antlrextraction, antlrmm);
		TargetDefinition antlrextractionTgt = new TargetDefinition(antlrextraction);
		antlrextraction.setOption("identEsc", "");

		
		ct = new CompositeTransformation();
		srckm3 = new CompositeTransformationSourceDefinition(ct, km3mm);
		srcannotation = new CompositeTransformationSourceDefinition(ct, annotationmm);
		srctcs = new CompositeTransformationSourceDefinition(ct, tcsmm);
		srcacg = new CompositeTransformationSourceDefinition(ct, acgmm);
		tgttcsproblem = new CompositeTransformationTargetDefinition(ct, problemmm);
		tgtkm3problem = new CompositeTransformationTargetDefinition(ct, problemmm);
		tgtacgproblem = new CompositeTransformationTargetDefinition(ct, problemmm);
		tgtasm = new CompositeTransformationTargetDefinition(ct, null);
		tgteditor = new CompositeTransformationTargetDefinition(ct, editormm);
		tgtoutline = new CompositeTransformationTargetDefinition(ct, outlinemm);
		tgtgrammar = new CompositeTransformationTargetDefinition(ct, antlrmm);
		tgtecore = new CompositeTransformationTargetDefinition(ct, ecoremm);
		tgtecorePlusAnnotations = new CompositeTransformationTargetDefinition(ct, ecoremm);
		tgttcs = new CompositeTransformationTargetDefinition(ct, tcsmm);

		// TCS Injection Usage
		TransformationUsage tcsinjectionUsage = new TransformationUsage(tcsinjection);
//		Source tcsinjectionUsageSrc =
			new Source(tcsinjectionUsage, tcsinjectionSrc, srctcs);
		Target tcsinjectionUsageTgt = new Target(tcsinjectionUsage, tcsinjectionTgt);
//		Target tcsinjectionUsageProblemTgt = new Target(tcsinjectionUsage, tcsinjectionProblemTgt);
		
		tgttcs.setSource(tcsinjectionUsageTgt);

//		tgttcssyntaxproblem.setSource(tcsinjectionUsageProblemTgt);

		// TCS2Editor Usage
		TransformationUsage tcs2editorUsage = new TransformationUsage(tcs2editor);
//		Source tcs2editorUsageSrc =
			new Source(tcs2editorUsage, tcs2editorSrc, tcsinjectionUsageTgt);
		Target tcs2editorUsageTgt = new Target(tcs2editorUsage, tcs2editorTgt);
		
		tgteditor.setSource(tcs2editorUsageTgt);

		// KM3 Injection Usage
		TransformationUsage km3injectionUsage = new TransformationUsage(km3injection);
//		Source km3injectionUsageSrc =
			new Source(km3injectionUsage, km3injectionSrc, srckm3);
		Target km3injectionUsageTgt = new Target(km3injectionUsage, km3injectionTgt);
//		Target km3injectionUsageProblemTgt = new Target(km3injectionUsage, km3injectionProblemTgt);
		
//		tgtkm3syntaxproblem.setSource(km3injectionUsageProblemTgt);

		// Annotation Injection Usage
		TransformationUsage annotationinjectionUsage = new TransformationUsage(annotationinjection);
//		Source annotation =
			new Source(annotationinjectionUsage, annotationinjectionSrc, srcannotation);
		Target annotationinjectionUsageTgt = new Target(annotationinjectionUsage, annotationinjectionTgt);

		// TCS2Problem Usage
		TransformationUsage tcs2problemUsage = new TransformationUsage(tcs2problem);
//		Source tcs2problemUsageTCSSrc =
			new Source(tcs2problemUsage, tcs2problemTCSSrc, tcsinjectionUsageTgt);
//		Source tcs2problemUsageKM3Src =
			new Source(tcs2problemUsage, tcs2problemKM3Src, km3injectionUsageTgt);
		Target tcs2problemUsageTgt = new Target(tcs2problemUsage, tcs2problemTgt);
		
		tgttcsproblem.setSource(tcs2problemUsageTgt);

		// KM32Problem Usage
		TransformationUsage km32problemUsage = new TransformationUsage(km32problem);
//		Source km32problemUsageSrc =
			new Source(km32problemUsage, km32problemSrc, km3injectionUsageTgt);
		Target km32problemUsageTgt = new Target(km32problemUsage, km32problemTgt);
		
		tgtkm3problem.setSource(km32problemUsageTgt);

		// KM32Outline Usage
		TransformationUsage km32outlineUsage = new TransformationUsage(km32outline);
//		Source km32outlineUsageSrc =
			new Source(km32outlineUsage, km32outlineSrc, km3injectionUsageTgt);
		Target km32outlineUsageTgt = new Target(km32outlineUsage, km32outlineTgt);
		
		tgtoutline.setSource(km32outlineUsageTgt);

		// ACG Injection Usage
		TransformationUsage acginjectionUsage = new TransformationUsage(acginjection);
//		Source acginjectionUsageSrc =
			new Source(acginjectionUsage, acginjectionSrc, srcacg);
		Target acginjectionUsageTgt = new Target(acginjectionUsage, acginjectionTgt);

		// ACG Compilation Usage
		TransformationUsage acgCompilationUsage = new TransformationUsage(acgCompilation);
//		Source acgCompilationUsageSrc =
			new Source(acgCompilationUsage, acgCompilationSrc, acginjectionUsageTgt);
		Target acgCompilationUsageTgt = new Target(acgCompilationUsage, acgCompilationTgt);
		
		tgtasm.setSource(acgCompilationUsageTgt);

		// KM32Ecore Usage
		TransformationUsage km32ecoreUsage = new TransformationUsage(km32ecore);
//		Source km32ecoreUsageSrc =
			new Source(km32ecoreUsage, km32ecoreSrc, km3injectionUsageTgt);
		Target km32ecoreUsageTgt = new Target(km32ecoreUsage, km32ecoreTgt);
		
		tgtecore.setSource(km32ecoreUsageTgt);

		// ApplyAnnotations2Ecore Usage
		TransformationUsage applyAnnotations2ecoreUsage = new TransformationUsage(applyAnnotations2ecore);
//		Source applyAnnotations2ecoreUsageSrc =
			new Source(applyAnnotations2ecoreUsage, applyAnnotations2ecoreSrc, km32ecoreUsageTgt);
//		Source applyAnnotations2ecoreUsageAnnotationSrc =
			new Source(applyAnnotations2ecoreUsage, applyAnnotations2ecoreAnnotationSrc, annotationinjectionUsageTgt);
		Target applyAnnotations2ecoreUsageTgt = new Target(applyAnnotations2ecoreUsage, applyAnnotations2ecoreTgt);
//		Target applyAnnotations2ecoreUsageRefiningTraceTgt =
			new Target(applyAnnotations2ecoreUsage, applyAnnotations2ecoreRefiningTraceTgt);

		tgtecorePlusAnnotations.setSource(applyAnnotations2ecoreUsageTgt);

		// TCS2ANTLR Usage
		TransformationUsage tcs2antlrUsage = new TransformationUsage(tcs2antlr);
//		Source tcs2antlrUsageTCSSrc =
			new Source(tcs2antlrUsage, tcs2antlrTCSSrc, tcsinjectionUsageTgt);
//		Source tcs2antlrUsageKM3Src =
			new Source(tcs2antlrUsage, tcs2antlrKM3Src, km3injectionUsageTgt);
		Target tcs2antlrUsageTgt = new Target(tcs2antlrUsage, tcs2antlrTgt);

		// ANTLR Extraction Usage
		TransformationUsage antlrextractionUsage = new TransformationUsage(antlrextraction);
//		Source antlrextractionUsageSrc =
			new Source(antlrextractionUsage, antlrextractionSrc, tcs2antlrUsageTgt);
		Target antlrextractionUsageTgt = new Target(antlrextractionUsage, antlrextractionTgt);
		
		tgtgrammar.setSource(antlrextractionUsageTgt);
	}
		
	private ResourceLocator rl;
	
	protected Metamodel getMetamodel(String name) throws IOException {
		return getMetamodel(name, "");
	}
	
	protected Metamodel getMetamodel(String name, String folderSuffix) throws IOException {
		return new Metamodel(name, rl.getResource(name + folderSuffix + "/Metamodel/" + name + ".ecore"));
	}
}

/**
 * Copyright (c) 2007, 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: TCSExtraction.java,v 1.5 2008/06/25 13:01:57 fjouault Exp $
 */
package org.eclipse.gmt.tcs.builder.ct.definition;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.gmt.tcs.builder.ct.Metamodel;
import org.eclipse.gmt.tcs.builder.ct.usage.Source;
import org.eclipse.gmt.tcs.builder.ct.usage.Target;
import org.eclipse.gmt.tcs.builder.ct.usage.TransformationUsage;
import org.eclipse.gmt.tcs.extractor.TCSExtractor;
import org.eclipse.m2m.atl.dsls.textsource.ByteArrayTextSource;
import org.eclipse.m2m.atl.engine.AtlEMFModelHandler;
import org.eclipse.m2m.atl.engine.AtlModelHandler;
import org.eclipse.m2m.atl.engine.extractors.Extractor;
import org.eclipse.m2m.atl.engine.vm.nativelib.ASMModel;

/**
 * 
 * @author Fr�d�ric Jouault
 *
 */
public class TCSExtraction extends Transformation {

	public void execute(TransformationUsage usage, Map models) throws IOException {
		Source source = (Source)usage.sources.get(0);
		ASMModel sourceModel = (ASMModel)source.origin.get(models);
		Extractor ext = new TCSExtractor();
		Map params = new HashMap();
		params.put("indentString", "    ");
		params.put("format", syntax);
		params.put("serializeComments", "true");
		Target target = (Target)usage.targets.get(0);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		params.putAll(options);
		ext.extract(sourceModel, baos, params);
		models.put(target, new ByteArrayTextSource(baos.toByteArray()));
	}
	
	private ASMModel syntax;
	
	public TCSExtraction(Metamodel tcsmm, URI syntaxURI) throws IOException {
		AtlEMFModelHandler amh = (AtlEMFModelHandler)AtlModelHandler.getDefault(AtlModelHandler.AMH_EMF);

		ASMModel tcsMetamodel = tcsmm.getMetamodel();
		syntax = amh.loadModel(tcsMetamodel.getName() + "-TCS.xmi", tcsMetamodel, syntaxURI);
	}

	public TCSExtraction(Metamodel tcsmm, URL syntaxURL) throws IOException {
		AtlEMFModelHandler amh = (AtlEMFModelHandler)AtlModelHandler.getDefault(AtlModelHandler.AMH_EMF);

		ASMModel tcsMetamodel = tcsmm.getMetamodel();
		InputStream in = syntaxURL.openStream();
		syntax = amh.loadModel(tcsMetamodel.getName() + "-TCS.xmi", tcsMetamodel, in);
		in.close();
	}

}

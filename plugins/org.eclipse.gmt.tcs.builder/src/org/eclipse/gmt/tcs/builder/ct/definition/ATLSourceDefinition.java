/**
 * Copyright (c) 2007 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: ATLSourceDefinition.java,v 1.2 2008/06/25 13:01:56 fjouault Exp $
 */
package org.eclipse.gmt.tcs.builder.ct.definition;

import org.eclipse.gmt.tcs.builder.ct.Metamodel;

/**
 * 
 * @author Fr�d�ric Jouault
 *
 */
public class ATLSourceDefinition extends ModelSourceDefinition {

	private String modelName;
	private String metamodelName;

	public ATLSourceDefinition(Transformation transformation, String modelName, String metamodelName, Metamodel metamodel) {
		super(transformation, metamodel);
		this.modelName = modelName;
		this.metamodelName = metamodelName;
	}
	
	public String getModelName() {
		return modelName;
	}

	public String getMetamodelName() {
		return metamodelName;
	}

}

/**
 * Copyright (c) 2007, 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: ATLTransformation.java,v 1.4 2008/06/25 13:01:57 fjouault Exp $
 */
package org.eclipse.gmt.tcs.builder.ct.definition;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.gmt.tcs.builder.ct.usage.Source;
import org.eclipse.gmt.tcs.builder.ct.usage.Target;
import org.eclipse.gmt.tcs.builder.ct.usage.TransformationUsage;
import org.eclipse.m2m.atl.dsls.textsource.TextSource;
import org.eclipse.m2m.atl.engine.AtlLauncher;
import org.eclipse.m2m.atl.engine.AtlModelHandler;
import org.eclipse.m2m.atl.engine.vm.ASM;
import org.eclipse.m2m.atl.engine.vm.ASMXMLReader;
import org.eclipse.m2m.atl.engine.vm.Debugger;
import org.eclipse.m2m.atl.engine.vm.SimpleDebugger;
import org.eclipse.m2m.atl.engine.vm.nativelib.ASMModel;

/**
 * 
 * @author Fr�d�ric Jouault
 *
 */
public class ATLTransformation extends Transformation {

	public void execute(TransformationUsage usage, Map contextualModels) throws IOException {
		Map libraries = new HashMap(this.libraries);
		AtlModelHandler amh = AtlModelHandler.getDefault(AtlModelHandler.AMH_EMF);

		Map models = new HashMap();
		models.put("MOF", amh.getMof());
		Map sourceModelBySourceDef = new HashMap();
		for(Iterator i = usage.sources.iterator() ; i.hasNext() ; ) {
			Source source = (Source)i.next();
			ATLSourceDefinition sourceDef = (ATLSourceDefinition)source.definition;
			ASMModel sourceModel = (ASMModel)source.origin.get(contextualModels);
			ASMModel sourceMetamodel = sourceDef.getMetamodel().getMetamodel();
			sourceModel.setIsTarget(false);
			models.put(sourceDef.getMetamodelName(), sourceMetamodel);
			models.put(sourceDef.getModelName(), sourceModel);
			sourceModelBySourceDef.put(sourceDef, sourceModel);
		}
		for(Iterator i = usage.targets.iterator() ; i.hasNext() ; ) {
			Target target = (Target)i.next();
			ASMModel targetModel = null;
			if(target.definition instanceof ATLTargetDefinition) {
				ATLTargetDefinition targetDef = (ATLTargetDefinition)target.definition;
				ASMModel targetMetamodel = targetDef.getMetamodel().getMetamodel();
				models.put(targetDef.getMetamodelName(), targetMetamodel);
				targetModel = amh.newModel(targetDef.getModelName(), "myTarget.xmi", targetMetamodel);
				models.put(targetDef.getModelName(), targetModel);
	
			} else {
				ATLRefinedTargetDefinition targetDef = (ATLRefinedTargetDefinition)target.definition;
				targetModel = (ASMModel)sourceModelBySourceDef.get(targetDef.getSourceDefinition());
			}
			contextualModels.put(target, targetModel);
		}
		
		List superimpose = Collections.EMPTY_LIST;
		
		Map options = Collections.EMPTY_MAP;
		
		Debugger debugger = new SimpleDebugger(
				/* step = */ "true".equals(options.get("step")),
				/* stepops = */ new ArrayList(),
				/* deepstepops = */ new ArrayList(),
				/* nostepops = */ new ArrayList(),
				/* deepnostepops = */ new ArrayList(),
				/* showStackTrace = */ true,
				"true".equals(options.get("showSummary")),
				"true".equals(options.get("profile")),
				"true".equals(options.get("continueAfterError"))
		);
//		debugger = new NetworkDebugger(6060, true);
		
		AtlLauncher.getDefault().launch(asm, libraries, models, Collections.EMPTY_MAP, superimpose, options, debugger);
	}

	private ASM asm;
	public Map libraries;
	
	public ATLTransformation(TextSource transformationTS) throws IOException {
		this(transformationTS, Collections.EMPTY_MAP);
	}
	
	public ATLTransformation(TextSource transformationTS, Map libraries) throws IOException {
		asm = new ASMXMLReader().read(transformationTS.openStream());
		
		this.libraries = new HashMap();
		for(Iterator i = libraries.keySet().iterator() ; i.hasNext() ; ) {
			Object key = i.next();
			addLibrary(key, (TextSource)libraries.get(key));
		}
	}
	
	public void addLibrary(Object name, TextSource ts) throws IOException {
		this.libraries.put(name, new ASMXMLReader().read(ts.openStream()));		
	}
	
	public ASM getASM() {
		return asm;
	}
}

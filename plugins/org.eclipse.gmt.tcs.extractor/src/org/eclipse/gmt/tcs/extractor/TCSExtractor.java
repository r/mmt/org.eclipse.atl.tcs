/**
 * Copyright (c) 2005, 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: TCSExtractor.java,v 1.4 2008/06/25 12:48:34 fjouault Exp $
 */
package org.eclipse.gmt.tcs.extractor;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.m2m.atl.engine.extractors.Extractor;
import org.eclipse.m2m.atl.engine.vm.nativelib.ASMModel;

/**
 * Created on 9 mars 2005
 * @author Fr�d�ric Jouault
 * @author Mika�l Barbero
 */
public class TCSExtractor implements Extractor {

	private static Map parameterTypes = new HashMap();
	
	static {
		parameterTypes.put("format", "Model:TCS");			// required
		parameterTypes.put("indentString", "String");		// optional, default = "  "
		parameterTypes.put("standardSeparator", "String");	// optional, default = " "
		parameterTypes.put("kwCheckIgnoreCase", "String");	// optional, default = false
		parameterTypes.put("identEsc", "String");			// optional, default = "\"", has priority over the two others below
		parameterTypes.put("identEscStart", "String");		// optional, default = "\""
		parameterTypes.put("identEscEnd", "String");		// optional, default = "\""
		parameterTypes.put("stringDelim", "String");		// optional, default = "\'"	
		parameterTypes.put("debug", "String");				// optional, default = false	
		parameterTypes.put("debugws", "String");			// optional, default = false	
		parameterTypes.put("serializeComments", "String");	// optional, default = true
		parameterTypes.put("usePrimitiveTemplates", "String");	// optional, default = false
		parameterTypes.put("decimalFormat", "String");		// optional, default = "0.##############"
		parameterTypes.put("stream", "TCSExtractorStream");// optional, default = new TCSExtractorPrintStream(target);
	}

	public Map getParameterTypes() {
		return parameterTypes;
	}
	
	public void extract(ASMModel source, OutputStream target, Map params) {
		ModelAdapter sourceModelAdapter = (ModelAdapter)params.get("modelAdapter");
		if(sourceModelAdapter == null) {
			sourceModelAdapter = new ASMModelAdapter();
		}
		new PrettyPrinter().prettyPrint(source, sourceModelAdapter, target, params);		
	}
	
	public void extract(ASMModel format, ASMModel extent, OutputStream out) {
		throw new UnsupportedOperationException("Was deprecated a long time ago. It is now unsupported");
	}

	public String getPrefix() {
		return "ebnf";
	}
}

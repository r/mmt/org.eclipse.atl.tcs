<?xml version = '1.0' encoding = 'ISO-8859-1' ?>
<asm name="0">
	<cp>
		<constant value="TCS2ANTLRActions"/>
		<constant value="reserved"/>
		<constant value="J"/>
		<constant value="literalDelim"/>
		<constant value="typeModelElement"/>
		<constant value="postActions"/>
		<constant value="nextTokenValue"/>
		<constant value="tokenType"/>
		<constant value="lexerFragmentKeyword"/>
		<constant value="lexerActionSetText"/>
		<constant value="lexerActionGetText"/>
		<constant value="lexerHeader"/>
		<constant value="lexerV2Replacement"/>
		<constant value="lexerV3Replacement"/>
		<constant value="errorReportingAction"/>
		<constant value="main"/>
		<constant value="A"/>
		<constant value="Sequence"/>
		<constant value="#native"/>
		<constant value="case"/>
		<constant value="CJ.including(J):CJ"/>
		<constant value="class"/>
		<constant value="clone"/>
		<constant value="final"/>
		<constant value="import"/>
		<constant value="interface"/>
		<constant value="package"/>
		<constant value="super"/>
		<constant value="void"/>
		<constant value="grammar"/>
		<constant value="J.union(J):J"/>
		<constant value="'"/>
		<constant value="Object"/>
		<constant value="if(input.LT(-1) != null) ei.setLocation(ret, firstToken.getLine() + &quot;:&quot; + (firstToken.getCharPositionInLine() + 1) + &quot;-&quot; + ((org.eclipse.gmt.tcs.injector.wrappers.antlr3.ANTLR3LocationToken)input.LT(-1)).getEndLine() + &quot;:&quot; + (((org.eclipse.gmt.tcs.injector.wrappers.antlr3.ANTLR3LocationToken)input.LT(-1)).getEndColumn() + 1));"/>
		<constant value="ei.setCommentsBefore(ret, new Object[] {input, firstToken});&#10;&#9;   ei.setCommentsAfter(ret, new Object[] {input, input.LT(-1)});"/>
		<constant value="J.+(J):J"/>
		<constant value="input.LT(1)"/>
		<constant value="org.antlr.runtime.Token"/>
		<constant value="fragment"/>
		<constant value="setText"/>
		<constant value="getText()"/>
		<constant value="&#10;&#9;"/>
		<constant value="//"/>
		<constant value=""/>
		<constant value="&#10;//&#9;public void reportError(RecognitionException ex) {&#10;//&#9;&#9;ei.reportError((Exception)ex);&#10;//&#9;}&#10;&#9;&#10;//&#9;public void emitErrorMessage(String s) {&#10;//&#9;&#9;ei.reportError(s);&#10;//&#9;}&#10;&#10;&#9;public void displayRecognitionError(String[] tokenNames, RecognitionException e) {&#10;&#9;&#9;ei.reportError((Exception)e);&#10;&#9;}&#10;&#10;&#9;//TODO: what about warnings?&#10;//&#9;public void reportWarning(String s) {&#10;//&#9;&#9;ei.reportWarning(s);&#10;//&#9;}&#10;&#9;"/>
		<constant value="5:12-5:18"/>
		<constant value="5:20-5:27"/>
		<constant value="5:29-5:36"/>
		<constant value="5:38-5:45"/>
		<constant value="5:47-5:55"/>
		<constant value="5:57-5:68"/>
		<constant value="5:70-5:79"/>
		<constant value="5:81-5:88"/>
		<constant value="5:90-5:96"/>
		<constant value="5:2-5:97"/>
		<constant value="6:13-6:22"/>
		<constant value="6:3-6:23"/>
		<constant value="5:2-7:3"/>
		<constant value="10:2-10:6"/>
		<constant value="14:2-14:10"/>
		<constant value="20:2-20:338"/>
		<constant value="23:5-24:67"/>
		<constant value="20:2-24:67"/>
		<constant value="27:2-27:15"/>
		<constant value="30:2-30:27"/>
		<constant value="37:2-37:12"/>
		<constant value="40:2-40:11"/>
		<constant value="43:2-43:13"/>
		<constant value="47:2-48:3"/>
		<constant value="51:2-51:6"/>
		<constant value="54:2-54:4"/>
		<constant value="68:2-85:3"/>
		<constant value="self"/>
		<constant value="createAction"/>
		<constant value="1"/>
		<constant value="2"/>
		<constant value="3"/>
		<constant value="(backtracking==0) ? ei.create(&quot;"/>
		<constant value="&quot;, "/>
		<constant value="J.toString():J"/>
		<constant value=", "/>
		<constant value=") : null"/>
		<constant value="34:2-34:36"/>
		<constant value="34:39-34:47"/>
		<constant value="34:2-34:47"/>
		<constant value="34:50-34:56"/>
		<constant value="34:2-34:56"/>
		<constant value="34:59-34:68"/>
		<constant value="34:59-34:79"/>
		<constant value="34:2-34:79"/>
		<constant value="34:82-34:86"/>
		<constant value="34:2-34:86"/>
		<constant value="34:89-34:103"/>
		<constant value="34:89-34:114"/>
		<constant value="34:2-34:114"/>
		<constant value="34:117-34:127"/>
		<constant value="34:2-34:127"/>
		<constant value="typeName"/>
		<constant value="isContext"/>
		<constant value="isAddToContext"/>
		<constant value="getGrammarOptions"/>
		<constant value="J.oclIsUndefined():J"/>
		<constant value="14"/>
		<constant value="0"/>
		<constant value="J.=(J):J"/>
		<constant value="12"/>
		<constant value="QJ.first():J"/>
		<constant value="13"/>
		<constant value="backtrack = true;&#10;"/>
		<constant value="18"/>
		<constant value="57:5-57:6"/>
		<constant value="57:5-57:23"/>
		<constant value="60:6-60:7"/>
		<constant value="60:10-60:11"/>
		<constant value="60:6-60:11"/>
		<constant value="63:4-63:16"/>
		<constant value="61:4-61:25"/>
		<constant value="60:3-64:8"/>
		<constant value="58:3-58:15"/>
		<constant value="57:2-65:7"/>
		<constant value="k"/>
	</cp>
	<field name="1" type="2"/>
	<field name="3" type="2"/>
	<field name="4" type="2"/>
	<field name="5" type="2"/>
	<field name="6" type="2"/>
	<field name="7" type="2"/>
	<field name="8" type="2"/>
	<field name="9" type="2"/>
	<field name="10" type="2"/>
	<field name="11" type="2"/>
	<field name="12" type="2"/>
	<field name="13" type="2"/>
	<field name="14" type="2"/>
	<operation name="15">
		<context type="16"/>
		<parameters>
		</parameters>
		<code>
			<getasm/>
			<push arg="17"/>
			<push arg="18"/>
			<new/>
			<push arg="19"/>
			<call arg="20"/>
			<push arg="21"/>
			<call arg="20"/>
			<push arg="22"/>
			<call arg="20"/>
			<push arg="23"/>
			<call arg="20"/>
			<push arg="24"/>
			<call arg="20"/>
			<push arg="25"/>
			<call arg="20"/>
			<push arg="26"/>
			<call arg="20"/>
			<push arg="27"/>
			<call arg="20"/>
			<push arg="28"/>
			<call arg="20"/>
			<push arg="17"/>
			<push arg="18"/>
			<new/>
			<push arg="29"/>
			<call arg="20"/>
			<call arg="30"/>
			<set arg="1"/>
			<getasm/>
			<push arg="31"/>
			<set arg="3"/>
			<getasm/>
			<push arg="32"/>
			<set arg="4"/>
			<getasm/>
			<push arg="33"/>
			<push arg="34"/>
			<call arg="35"/>
			<set arg="5"/>
			<getasm/>
			<push arg="36"/>
			<set arg="6"/>
			<getasm/>
			<push arg="37"/>
			<set arg="7"/>
			<getasm/>
			<push arg="38"/>
			<set arg="8"/>
			<getasm/>
			<push arg="39"/>
			<set arg="9"/>
			<getasm/>
			<push arg="40"/>
			<set arg="10"/>
			<getasm/>
			<push arg="41"/>
			<set arg="11"/>
			<getasm/>
			<push arg="42"/>
			<set arg="12"/>
			<getasm/>
			<push arg="43"/>
			<set arg="13"/>
			<getasm/>
			<push arg="44"/>
			<set arg="14"/>
		</code>
		<linenumbertable>
			<lne id="45" begin="4" end="4"/>
			<lne id="46" begin="6" end="6"/>
			<lne id="47" begin="8" end="8"/>
			<lne id="48" begin="10" end="10"/>
			<lne id="49" begin="12" end="12"/>
			<lne id="50" begin="14" end="14"/>
			<lne id="51" begin="16" end="16"/>
			<lne id="52" begin="18" end="18"/>
			<lne id="53" begin="20" end="20"/>
			<lne id="54" begin="1" end="21"/>
			<lne id="55" begin="25" end="25"/>
			<lne id="56" begin="22" end="26"/>
			<lne id="57" begin="1" end="27"/>
			<lne id="58" begin="30" end="30"/>
			<lne id="59" begin="33" end="33"/>
			<lne id="60" begin="36" end="36"/>
			<lne id="61" begin="37" end="37"/>
			<lne id="62" begin="36" end="38"/>
			<lne id="63" begin="41" end="41"/>
			<lne id="64" begin="44" end="44"/>
			<lne id="65" begin="47" end="47"/>
			<lne id="66" begin="50" end="50"/>
			<lne id="67" begin="53" end="53"/>
			<lne id="68" begin="56" end="56"/>
			<lne id="69" begin="59" end="59"/>
			<lne id="70" begin="62" end="62"/>
			<lne id="71" begin="65" end="65"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="72" begin="0" end="66"/>
		</localvariabletable>
	</operation>
	<operation name="73">
		<context type="16"/>
		<parameters>
			<parameter name="74" type="2"/>
			<parameter name="75" type="2"/>
			<parameter name="76" type="2"/>
		</parameters>
		<code>
			<push arg="77"/>
			<load arg="74"/>
			<call arg="35"/>
			<push arg="78"/>
			<call arg="35"/>
			<load arg="75"/>
			<call arg="79"/>
			<call arg="35"/>
			<push arg="80"/>
			<call arg="35"/>
			<load arg="76"/>
			<call arg="79"/>
			<call arg="35"/>
			<push arg="81"/>
			<call arg="35"/>
		</code>
		<linenumbertable>
			<lne id="82" begin="0" end="0"/>
			<lne id="83" begin="1" end="1"/>
			<lne id="84" begin="0" end="2"/>
			<lne id="85" begin="3" end="3"/>
			<lne id="86" begin="0" end="4"/>
			<lne id="87" begin="5" end="5"/>
			<lne id="88" begin="5" end="6"/>
			<lne id="89" begin="0" end="7"/>
			<lne id="90" begin="8" end="8"/>
			<lne id="91" begin="0" end="9"/>
			<lne id="92" begin="10" end="10"/>
			<lne id="93" begin="10" end="11"/>
			<lne id="94" begin="0" end="12"/>
			<lne id="95" begin="13" end="13"/>
			<lne id="96" begin="0" end="14"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="72" begin="0" end="14"/>
			<lve slot="1" name="97" begin="0" end="14"/>
			<lve slot="2" name="98" begin="0" end="14"/>
			<lve slot="3" name="99" begin="0" end="14"/>
		</localvariabletable>
	</operation>
	<operation name="100">
		<context type="16"/>
		<parameters>
			<parameter name="74" type="2"/>
		</parameters>
		<code>
			<load arg="74"/>
			<call arg="101"/>
			<if arg="102"/>
			<load arg="74"/>
			<pushi arg="103"/>
			<call arg="104"/>
			<if arg="105"/>
			<push arg="17"/>
			<push arg="18"/>
			<new/>
			<call arg="106"/>
			<goto arg="107"/>
			<push arg="108"/>
			<goto arg="109"/>
			<push arg="17"/>
			<push arg="18"/>
			<new/>
			<call arg="106"/>
		</code>
		<linenumbertable>
			<lne id="110" begin="0" end="0"/>
			<lne id="111" begin="0" end="1"/>
			<lne id="112" begin="3" end="3"/>
			<lne id="113" begin="4" end="4"/>
			<lne id="114" begin="3" end="5"/>
			<lne id="115" begin="7" end="10"/>
			<lne id="116" begin="12" end="12"/>
			<lne id="117" begin="3" end="12"/>
			<lne id="118" begin="14" end="17"/>
			<lne id="119" begin="0" end="17"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="72" begin="0" end="17"/>
			<lve slot="1" name="120" begin="0" end="17"/>
		</localvariabletable>
	</operation>
</asm>

/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: DSLResourceProvider.java,v 1.4 2008/06/25 12:50:28 fjouault Exp $
 */
package org.eclipse.gmt.tcs.dsls;

import org.osgi.framework.Bundle;

/**
 * 
 * @author Fr�d�ric Jouault
 * @author Mika�l Barbero
 *
 */
public class DSLResourceProvider extends
		org.eclipse.m2m.atl.dsls.DSLResourceProvider {
	
	public Bundle getBundle() {
		return Activator.getDefault().getBundle();
	}

	public String getPluginId() {
		return Activator.PLUGIN_ID;
	}
}

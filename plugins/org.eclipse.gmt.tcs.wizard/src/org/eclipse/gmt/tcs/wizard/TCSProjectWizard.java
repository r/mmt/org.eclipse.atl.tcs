/**
 * Copyright (c) 2007, 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: TCSProjectWizard.java,v 1.8 2008/07/03 14:43:34 fjouault Exp $
 */
package org.eclipse.gmt.tcs.wizard;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.gmt.tcs.builder.TCSBuilder;
import org.eclipse.gmt.tcs.metadata.LanguageRegistry;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;

/**
 * 
 * @author Fr�d�ric Jouault
 * @author Mika�l Barbero
 *
 */
public class TCSProjectWizard extends Wizard implements INewWizard {

	// TODO create TCS-specific nature
	private static final String ATL_NATURE_ID = "org.eclipse.m2m.atl.adt.builder.atlNature";
	private TCSProjectWizardPage page;

	public void addPages() {
		page = new TCSProjectWizardPage(null);
		addPage(page);
	}

	public TCSProjectWizard() {
		// TODO Auto-generated constructor stub
	}

	public boolean performFinish() {
		String name = page.getProjectName();
		String ext = page.getExtension();

		try{
			// Get the worskspace container (IWorkspaceRoot)
			IWorkspace wks = ResourcesPlugin.getWorkspace();
			IWorkspaceRoot wksroot = wks.getRoot();

			// Create a project model instance
			IProject project = wksroot.getProject(name);
			if(!project.exists())
				project.create(null);

			// open project if necessary
			if(!project.isOpen())
				project.open(null);

			createFolder(project, "Metamodel");
			Map subst = new HashMap();
			subst.put("dsl.name", name);
			subst.put("dsl.ext", ext);
			subst.put("author", System.getProperty("user.name"));
			subst.put("date", new SimpleDateFormat("yyyy/MM/dd").format(new Date()));

			createFile(project, "Metamodel/" + name + ".km3",
				TCSBuilder.getTemplate("templates/metamodel.km3", subst)
			);
			createFile(project, "Metamodel/" + name + ".ann",
				TCSBuilder.getTemplate("templates/annotations.ann", subst)
			);

			createFolder(project, "Syntax");
			createFile(project, "Syntax/" + name + ".tcs",
				TCSBuilder.getTemplate("templates/syntax.tcs", subst)
			);
			createFile(project, "build.properties",
				TCSBuilder.getTemplate("templates/build.properties", subst)
			);

			createFolder(project, "Samples");
			createFile(project, "Samples/Test1." + ext, "{\n\ta,\n\tb\n}");

			addNature(project, TCSProjectWizard.ATL_NATURE_ID);
			addBuilder(project, TCSBuilder.ID);
			LanguageRegistry.getDefault().getLanguageFromSource(project);


/* TODO: support Plugin projects
			createFolder(project, "META-INF");
			createFile(project, "META-INF/MANIFEST.MF", TCSBuilder.getTemplate("templates/MANIFEST.MF", subst));
			createFile(project, "plugin.xml", TCSBuilder.getTemplate("templates/plugin.xml", subst));
			addNature(project, JavaCore.NATURE_ID);
			IJavaProject javaProject = JavaCore.create(project);
			IFolder parserSrcFolder = createFolder(project, "Syntax/parser_src");
			IFolder parserBinFolder = project.getFolder("Syntax/parser_bin");
			IClasspathEntry sourceEntry = JavaCore.newSourceEntry(parserSrcFolder.getFullPath(), new IPath[] {}, parserBinFolder.getFullPath());
//			sourceEntry.
			javaProject.setRawClasspath(new IClasspathEntry[] {
					sourceEntry,
					JavaRuntime.getDefaultJREContainerEntry()
				}, null);
			
			//javaProject.setOutputLocation(path, null);
*/
		}
		catch(CoreException ce){
			System.out.println("core exception caught..." + ce.getMessage());
		} catch (IOException e) {
			System.out.println("io exception caught..." + e.getMessage());
		}

		return true;
	}
	
	private IFolder createFolder(IProject project, String path) throws CoreException {
		IFolder folder = project.getFolder(path);
		if(!folder.exists())
			folder.create(true, true, null);
		return folder;
	}

	private IFile createFile(IProject project, String path, String contents) throws CoreException {
		IFile file = project.getFile(path);
		if(!file.exists())
			file.create(new ByteArrayInputStream(contents.getBytes()), true, null);
		return file;
	}

	public void init(IWorkbench workbench, IStructuredSelection selection) {
		// TODO Auto-generated method stub

	}

	/**
	 * Adds a nature to a project
	 * @param project the project to add a nature to
	 * @param natureId the natureId of the nature to be added to the project
	 */
	public void addNature(IProject project, String natureId) {
	try {
	      IProjectDescription description = project.getDescription();
	      String[] natures = description.getNatureIds();
	      String[] newNatures = new String[natures.length + 1];
	      System.arraycopy(natures, 0, newNatures, 0, natures.length);
	      newNatures[natures.length] = natureId;
	      description.setNatureIds(newNatures);
	      project.setDescription(description, null);
	   } catch (CoreException e) {
	   		System.err.println(e);
	   }
	}
	
	private void addBuilder(IProject project, String id) throws CoreException {
      IProjectDescription desc = project.getDescription();
      ICommand[] commands = desc.getBuildSpec();
      for (int i = 0; i < commands.length; ++i)
         if (commands[i].getBuilderName().equals(id))
            return;
      //add builder to project
      ICommand command = desc.newCommand();
      command.setBuilderName(id);
      ICommand[] nc = new ICommand[commands.length + 1];
      // Add it before other builders.
      System.arraycopy(commands, 0, nc, 1, commands.length);
      nc[0] = command;
      desc.setBuildSpec(nc);
      project.setDescription(desc, null);
	}
}

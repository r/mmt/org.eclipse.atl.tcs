/**
 * Copyright (c) 2015 ESEO.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     ESEO - initial API and implementation
 *
 * $Id$
 */
package org.eclipse.gmt.tcs.metadata.adhoc;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmt.tcs.extractor.ModelAdapter;
import org.eclipse.m2m.atl.drivers.emf4atl.ASMEMFModelElement;
import org.eclipse.m2m.atl.engine.emfvm.StackFrame;
import org.eclipse.m2m.atl.engine.emfvm.lib.ASMModule;
import org.eclipse.m2m.atl.engine.emfvm.lib.EnumLiteral;
import org.eclipse.m2m.atl.engine.emfvm.lib.ExecEnv;
import org.eclipse.m2m.atl.engine.emfvm.lib.OclUndefined;
import org.eclipse.m2m.atl.engine.emfvm.lib.Operation;
import org.eclipse.m2m.atl.engine.vm.nativelib.ASMEnumLiteral;
import org.eclipse.m2m.atl.engine.vm.nativelib.ASMModelElement;
import org.eclipse.m2m.atl.engine.vm.nativelib.ASMOclUndefined;
import org.eclipse.m2m.atl.engine.vm.nativelib.ASMString;

/**
 * This ModelAdapter delegates to another ModelAdapter but catches access to non-existing properties, which it virtualizes.
 * 
 * @author Fr�d�ric Jouault
 */
public class VirtualExtractorModelAdapter implements ModelAdapter {

	private ModelAdapter actualModelAdapter;

	private ExecEnv execEnv;
	private ASMModule asmModule;

	public VirtualExtractorModelAdapter(ModelAdapter actualModelAdapter, ExecEnv execEnv, ASMModule asmModule) {
		this.actualModelAdapter = actualModelAdapter;
		this.execEnv = execEnv;
		this.asmModule = asmModule;
	}

	public Object get(Object ame, String propertyName) {
		if(ame instanceof ASMEMFModelElement) {
			ame = ((ASMEMFModelElement)ame).getObject();
		}
		Operation op = execEnv.getOperation(((EObject)ame).eClass(), "get");
		StackFrame stackFrame = new StackFrame(this.execEnv, this.asmModule, op);
		Object localVars[] = new Object[op.getMaxLocals()];
		localVars[0] = ame;
		localVars[1] = propertyName;
		stackFrame.setLocalVars(localVars);
		Object ret = op.exec(stackFrame);
		if(ret instanceof OclUndefined || ret instanceof ASMOclUndefined) {
			ret = null;
		}
		return ret;
	}

	public String getString(Object me, String propName) {
		Object ret = this.get(me, propName);
		if(ret instanceof EnumLiteral) {
			ret = this.getEnumLiteralName(ret);
		}
		return (String)ret;
	}

	public boolean getBool(Object me, String propName) {
		return ((Boolean)this.get(me, propName)).booleanValue();
	}

	public boolean getBoolUndefinedIsFalse(Object me, String propName) {
		Boolean ret = (Boolean)this.get(me, propName);
		return (ret == null) ? false : ret.booleanValue();
	}

	public int getInt(Object me, String propName) {
		return ((Number)this.get(me, propName)).intValue();
	}

	public Iterator getCol(Object me, String propName) {
		return ((Collection)this.get(me, propName)).iterator();
	}

	public Object getME(Object me, String propName) {
		return this.get(me, propName);
	}

	public String getName(Object me) {
		return this.getString(me, "name");
	}

	public String getEnumLiteralName(Object me) {
		if(me instanceof ASMEnumLiteral) {
			return ((ASMEnumLiteral)me).getName();
		} else {
			return (String)((EnumLiteral)me).get(null, "name");
		}
	}

	public Set getElementsByType(Object model, String typeName) {
		return this.actualModelAdapter.getElementsByType(model, typeName);
	}

	public String getTypeName(Object me) {
		if(me instanceof EObject) {
			return ((EObject)me).eClass().getName();
		} else {
			return this.actualModelAdapter.getTypeName(me);
		}
	}

	public Object getMetaobject(Object me) {
		if(me instanceof EObject) {
			return ((EObject)me).eClass();
		}
		return this.actualModelAdapter.getMetaobject(me);
	}

	public Object refImmediateComposite(Object me) {
		return this.actualModelAdapter.refImmediateComposite(me);
	}

	public boolean isAModelElement(Object o) {
		return o instanceof EObject || o instanceof ASMModelElement;
	}

	public Object getPropertyType(Object f, String propName) {
		if(f instanceof EClass) {
			return ((EClass)f).getEStructuralFeature(propName).getEType();
		}
		return this.actualModelAdapter.getPropertyType(f, propName);
	}

	public boolean isPrimitive(Object value) {
		return this.actualModelAdapter.isPrimitive(value);
	}

	public boolean isEnumLiteral(Object value) {
		return (value instanceof ASMEnumLiteral) || value instanceof EnumLiteral;
	}

	public String nextString(Iterator i) {
		Object ret = i.next();
		if(ret instanceof ASMString)
			ret = ((ASMString)ret).getSymbol();
		return (String)ret;
	}
}

/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: VMLauncher.java,v 1.2 2008/04/16 20:19:35 fjouault Exp $
 */
package org.eclipse.gmt.tcs.metadata;

import java.util.Map;

/**
 * @author Fr�d�ric Jouault
 *
 */
public interface VMLauncher {

	public Object launch(Object asm, Map models, Map libraries);
}

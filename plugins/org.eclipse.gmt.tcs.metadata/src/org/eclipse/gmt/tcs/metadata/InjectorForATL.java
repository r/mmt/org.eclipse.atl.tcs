/**
 * Copyright (c) 2014 ESEO.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     ESEO - initial API and implementation
 *
 * $Id$
 */
package org.eclipse.gmt.tcs.metadata;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.m2m.atl.core.ATLCoreException;
import org.eclipse.m2m.atl.core.IInjector;
import org.eclipse.m2m.atl.core.IModel;
import org.eclipse.m2m.atl.core.emf.EMFInjector;
import org.eclipse.m2m.atl.core.emf.EMFModel;
import org.eclipse.m2m.atl.drivers.emf4atl.ASMEMFModel;
import org.eclipse.m2m.atl.drivers.emf4atl.EMFModelLoader;
import org.eclipse.m2m.atl.dsls.textsource.TextSource;
import org.eclipse.m2m.atl.engine.vm.AtlModelHandler;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * 
 * @author Fr�d�ric Jouault
 */
public class InjectorForATL implements IInjector {

	public void inject(IModel targetModel, String source)
			throws ATLCoreException {
		this.inject(targetModel, source, new HashMap());
	}

	public void inject(IModel targetModel, String source, Map params) throws ATLCoreException {
		final URI uri = URI.createURI(source);

		// TODO: get language by name rather than by extension? this is currently unsupported by LanguageRegistry
		Language language = LanguageRegistry.getDefault().getLanguage(uri.fileExtension());

		// Passing ml=null to ASMEMFModel to make sure it will not unload the resource when garbage collected.
		EMFModelLoader ml = null;
		ModelFactory factory = ASMModelFactory.getDefault();

		String extension = "xmi";
		if(language.getSource() != null) {
			String ext = language.getSource().getProperty("model.ext");
			if(ext != null) {
				extension = ext;
			}
		}

		// ((EMFModel)targetModel).getResource() is not necessarily initialized yet.
		// If we create an ASMEMFModel in a standard way, it will have a resource, however
		// that resource will be dropped as soon as the (temporary) ASMEMFModel is dropped/garbage collected.
		// We therefore obtain a resource from the model factory of the target model.
		// Moreover, we must use targetModel.getReferenceModel(), and not language.getMetamodel(factory).
		// It is the responsibility of the caller to make sure that it is the right metamodel.
		Resource resource =
				((EMFModel)targetModel).getModelFactory().getResourceSet().createResource(URI.createFileURI("model." + extension));
		resource.setURI(uri); // set actual URI so that dependencies (e.g., other resources) may be resolved
		resource.getResourceSet().getLoadOptions().put(XMLResource.OPTION_RECORD_UNKNOWN_FEATURE, Boolean.TRUE);
		UMLUtil.init(resource.getResourceSet());
		ASMEMFModel model = new ASMEMFModel("model", resource,
					new ASMEMFModel(language.getName(), ((EMFModel)targetModel.getReferenceModel()).getResource(), (ASMEMFModel)ASMEMFModel.getMOF(), false, ml) {},
				true, ml) {
		};

		// For resolution to happen, WFR must be executed, for which we need the two following things:
		params.put("vmLauncher", new RegularVMLauncher());
		if(!params.containsKey("problems")) {
			params.put("problems", factory.newModel("pbs", AtlModelHandler.getDefault(AtlModelHandler.AMH_EMF).createModelLoader().getBuiltInMetaModel("Problem.ecore")));
		}

		language.inject(factory, model, new TextSource() {
			public InputStream openStream() throws IOException {
				return new URL(uri.toString()).openStream();
			}
		}, params);

		// put our Resource in targetModel 
		new EMFInjector().inject(targetModel, resource);
	}

	public void inject(IModel targetModel, InputStream source, Map options)
			throws ATLCoreException {
		throw new RuntimeException("injecting: " + source + " with options: " + options + " is not supported yet, inject from URI");
	}

}

/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: TCSInjection.java,v 1.3 2008/06/28 17:12:56 fjouault Exp $
 */
package org.eclipse.gmt.tcs.metadata.adhoc;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Map;

import org.eclipse.gmt.tcs.injector.TCSInjector;
import org.eclipse.gmt.tcs.metadata.ModelFactory;
import org.eclipse.m2m.atl.dsls.textsource.TextSource;
import org.eclipse.m2m.atl.engine.vm.nativelib.ASMModel;

/**
 * 
 * @author Fr�d�ric Jouault
 */
public class TCSInjection {

//	/**
//	 * 
//	 * @param uriJar
//	 * @return an URLClassLoader containing
//	 */
//	private static ClassLoader createLoader(URL jarURL) {
//		return new TCSClassLoader(new URL[] {jarURL}, TCSInjection.class.getClassLoader());
//	}
	
	public static Object inject(ModelFactory factory, Object model, Object metamodel, TextSource source, Map params, URL parserURL) {
		if(model == null) {
			model = factory.newModel("model.xmi", metamodel);
		}
//		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		try {
//			ClassLoader jcl = createLoader(parserURL);
//			Thread.currentThread().setContextClassLoader(jcl);
			InputStream in = new BufferedInputStream(source.openStream());
			params.put("extraClasspath", new URL[] {parserURL});
			new TCSInjector().inject((ASMModel)model, in, params);
			in.close();
		} catch(IOException e) {
			throw new RuntimeException("Could not inject", e);
		} finally {
//			Thread.currentThread().setContextClassLoader(cl);
		}
		return model;
	}
}

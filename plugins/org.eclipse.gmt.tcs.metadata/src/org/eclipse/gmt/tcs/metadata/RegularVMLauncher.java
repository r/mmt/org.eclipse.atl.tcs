/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: RegularVMLauncher.java,v 1.2 2008/04/16 20:19:35 fjouault Exp $
 */
package org.eclipse.gmt.tcs.metadata;

import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.m2m.atl.engine.vm.AtlLauncher;

/**
 * @author Fr�d�ric Jouault
 *
 */
public class RegularVMLauncher implements VMLauncher {

	public Object launch(Object asm, Map models, Map libraries) {
		Map asmParams = Collections.EMPTY_MAP;
		List superimpose = Collections.EMPTY_LIST;
		Map options = Collections.EMPTY_MAP;
		if(asm instanceof URL) {
			return AtlLauncher.getDefault().launch((URL)asm, libraries, models, asmParams, superimpose, options);
		} else {
			throw new RuntimeException("unsupported kind of asm: " + asm.getClass().getName());
		}
	}

}

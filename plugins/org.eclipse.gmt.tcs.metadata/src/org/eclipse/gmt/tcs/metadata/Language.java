/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: Language.java,v 1.2 2008/04/08 15:03:26 fjouault Exp $
 */
package org.eclipse.gmt.tcs.metadata;

import java.io.OutputStream;
import java.util.Map;

import org.eclipse.m2m.atl.dsls.textsource.TextSource;
import org.eclipse.m2m.atl.engine.vm.ASM;

/**
 * 
 * @author Fr�d�ric Jouault
 */
public interface Language {

	public String getName();

	public String getExtension();

	public Object getOutlineModel(ModelFactory factory);

	public Object getEditorModel(ModelFactory factory);

	public Object getMetamodel(ModelFactory factory);

	/**
	 * 
	 * @return the ASM of the compiler, or null if none exists.
	 */
	public ASM getCompiler();

	/**
	 * 
	 * @param factory
	 * @param model the model to populate or null, in which case a new model will
	 * be created using the factory.
	 * @param source
	 * @param params
	 * @return the injected model.
	 */
	public Object inject(ModelFactory factory, Object model, TextSource source, Map params);

	public void extract(ModelFactory factory, Object model, OutputStream out, Map params);

	/**
	 * @return the LanguageSource from which this Language
	 * is compiled, or null if no source is available (e.g., when the
	 * Language is provided by a plugin.
	 */
	public LanguageSource getSource();
}

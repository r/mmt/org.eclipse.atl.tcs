/**
 * Copyright (c) 2014 ESEO.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     ESEO - initial API and implementation
 *
 * $Id$
 */
package org.eclipse.gmt.tcs.metadata;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.m2m.atl.core.ATLCoreException;
import org.eclipse.m2m.atl.core.IExtractor;
import org.eclipse.m2m.atl.core.IModel;
import org.eclipse.m2m.atl.core.emf.EMFModel;
import org.eclipse.m2m.atl.drivers.emf4atl.ASMEMFModel;

/**
 * 
 * @author Fr�d�ric Jouault
 */
public class ExtractorForATL implements IExtractor {

	public final static String PARAM_LANGUAGE = "language";

	public void extract(IModel sourceModel, String target)
			throws ATLCoreException {
		this.extract(sourceModel, target, new HashMap());
	}

	public void extract(IModel sourceModel, String target, Map options)
			throws ATLCoreException {
		if(!options.containsKey(ExtractorForATL.PARAM_LANGUAGE)) {
			String extension = target.replaceAll("^.*\\.([^.]*)$", "$1");
			options.put(ExtractorForATL.PARAM_LANGUAGE, extension);
		}
		try {
			URL url = new URL(target);
			File file;
			try {
			  file = new File(url.toURI());
			} catch(URISyntaxException use) {
			  file = new File(url.getPath());
			}
			OutputStream out = new BufferedOutputStream(new FileOutputStream(file));
			this.extract(sourceModel, out, options);
			out.close();
		} catch(IOException ioe) {
			throw new RuntimeException("could not extract model to: " + target, ioe);
		}
	}

	public void extract(IModel sourceModel, OutputStream target, Map options)
			throws ATLCoreException {
		Object languageParam =  options.get(PARAM_LANGUAGE);
		if(languageParam == null) {
			throw new RuntimeException("language is neither specified nor can be infered");
		}
		Language language = null;
		if(languageParam instanceof Language) {
			language = (Language)languageParam;
		} else if(languageParam instanceof String) {
			language = LanguageRegistry.getDefault().getLanguage((String)languageParam);
			if(language == null) {
				throw new RuntimeException("no language found for extension: " + languageParam);
			}
		} else {
			throw new RuntimeException("cannot interpret language: " + languageParam);
		}
		// with modelLoader=null we ensure resources do not get unloaded when ASMEMFModel is finalized
		ASMEMFModel model = new ASMEMFModel("model", ((EMFModel)sourceModel).getResource(),
				new ASMEMFModel(language.getName(), ((EMFModel)sourceModel.getReferenceModel()).getResource(), (ASMEMFModel)ASMEMFModel.getMOF(), false, null) {},
			true, null) {
		};
		language.extract(ASMModelFactory.getDefault(), model, target, options);
	}
}

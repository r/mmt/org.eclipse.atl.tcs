/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: LanguageSource.java,v 1.3 2008/06/24 14:27:32 fjouault Exp $
 */
package org.eclipse.gmt.tcs.metadata;

import org.eclipse.core.resources.IFile;

/**
 * 
 * @author Fr�d�ric Jouault
 */
public interface LanguageSource {

	public IFile getTCSSourceFile();
	
	public IFile getKM3SourceFile();

	public IFile getAnnotationSourceFile();

	public String getProperty(String key);

	public IFile getEditorFile();

	public IFile getOutlineFile();

	public IFile getMetamodelFile();

	public IFile getGrammarFile();

	public String getParserGenerator();

	public IFile getParserFile();

	public IFile getACGSourceFile();

	public IFile getCompilerFile();
}

/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: LanguageRegistry.java,v 1.1 2008/04/01 17:47:40 fjouault Exp $
 */
package org.eclipse.gmt.tcs.metadata;

import org.eclipse.core.resources.IResource;
import org.eclipse.gmt.tcs.metadata.adhoc.AdHocLanguageRegistry;

/**
 * 
 * @author Fr�d�ric Jouault
 */
public abstract class LanguageRegistry {

	private static LanguageRegistry DEFAULT = new AdHocLanguageRegistry();

	public static LanguageRegistry getDefault() {
		return DEFAULT;
	}

	/**
	 * To be used by any Language user (e.g., TGE).
	 * TODO: Handle multiple Languages per extension.
	 */
	public abstract Language getLanguage(String extension);

	/**
	 * To be used by TCS builder.
	 * TODO: Handle multiple Languages per file
	 * (e.g., if resource is a KM3 metamodel, it may be associated to
	 * several TCS models).
	 * @param resource the resource (e.g., KM3 or TCS IFile,
	 * TCS Language IProject) identifying one of the source resource of the
	 * Language.
	 * @return the Language of which resource is a part of the source,
	 * or null if there is no corresponding Language. 
	 */
	public abstract Language getLanguageFromSource(IResource resource);
}

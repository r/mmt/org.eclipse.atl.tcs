/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: AdHocLanguageRegistry.java,v 1.4 2008/04/08 15:08:10 fjouault Exp $
 */
package org.eclipse.gmt.tcs.metadata.adhoc;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.gmt.tcs.metadata.Language;
import org.eclipse.gmt.tcs.metadata.LanguageRegistry;
import org.eclipse.gmt.tcs.metadata.LanguageRegistryListener;
import org.osgi.framework.Bundle;

/**
 * 
 * @author Fr�d�ric Jouault
 */
public class AdHocLanguageRegistry extends LanguageRegistry {
	
	private boolean traversedAllProjects = false;
	private Map languageByExtension;
	private Map languageBySourceProject;
	private List listeners = new ArrayList();

	public AdHocLanguageRegistry() {
		languageByExtension = new HashMap();

		IExtensionRegistry registry = Platform.getExtensionRegistry();
        if (registry != null) {
	        IExtensionPoint point = registry.getExtensionPoint("org.eclipse.gmt.tcs.metadata.registryListener");//$NON-NLS-1$
	
	        IExtension[] extensions = point.getExtensions();		
			for(int i = 0 ; i < extensions.length ; i++){		
				IConfigurationElement[] elements = extensions[i].getConfigurationElements();
				for(int j = 0 ; j < elements.length ; j++){
					try {
						LanguageRegistryListener registryListener = (LanguageRegistryListener)elements[j].createExecutableExtension("class");
						listeners.add(registryListener);
					} catch (CoreException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			 }
	
			point = registry.getExtensionPoint("org.eclipse.gmt.tcs.metadata.language");//$NON-NLS-1$
	
			extensions = point.getExtensions();		
			for(int i = 0 ; i < extensions.length ; i++){		
				IConfigurationElement[] elements = extensions[i].getConfigurationElements();
				for(int j = 0 ; j < elements.length ; j++){
					String pluginId = elements[j].getContributor().getName();
					Bundle bundle = Platform.getBundle(pluginId);
					String extension = elements[j].getAttribute("extension");
					String name = elements[j].getAttribute("name");
					URI metamodel = getURI(bundle, elements[j].getAttribute("metamodel"));
					URL editor = getURL(bundle, elements[j], "editor");
					URL outline = getURL(bundle, elements[j], "outline");
					URL syntax = getURL(bundle, elements[j], "syntax");
					URL parser = getURL(bundle, elements[j], "parser");
					Language language = new PluginLanguage(
						extension, name, metamodel, syntax, editor, outline, parser
					);
					languageByExtension.put(extension, language);
					register(language);
				}
			}
        }
		languageBySourceProject = new HashMap();
	}

	private URL getURL(Bundle bundle, IConfigurationElement element, String name) {
		return FileLocator.find(bundle, new Path(element.getAttribute(name)), Collections.EMPTY_MAP);
	}

	private URI getURI(Bundle bundle, String path) {
		// The following line is not compatible with Eclipse 2.2.x
		//return URI.createPlatformPluginURI(pluginId + "/" + path, true);
		return URI.createURI(FileLocator.find(bundle, new Path(path), Collections.EMPTY_MAP).toString());
	}

	public Language getLanguage(String extension) {
		Language ret = (Language)languageByExtension.get(extension);

		IWorkspace workspace = null;
		try {
			workspace = ResourcesPlugin.getWorkspace();

			if((ret == null) && !traversedAllProjects) {
				IProject projects[] = workspace.getRoot().getProjects();
	
				LanguageRegistry tsr = LanguageRegistry.getDefault();
				for(int i = 0 ; i < projects.length ; i++) {
					Language ts = tsr.getLanguageFromSource(projects[i]);
					// ts == null if the project is not a language project or is closed
					if(ts != null) {
						register(ts);
					}
				}
				traversedAllProjects = true;
				ret = (Language)languageByExtension.get(extension);
			}
		} catch(IllegalStateException e) {}

		return ret;
	}

	public Language getLanguageFromSource(IResource resource) {
		IProject project = resource.getProject();
		Language ret = (Language)languageBySourceProject.get(project);

		if(ret == null) {
			try {
				ret = new WorkspaceLanguage(project);
				languageBySourceProject.put(project, ret);

				Language other = (Language)languageByExtension.get(ret.getExtension());
				try {
					project.deleteMarkers(IMarker.PROBLEM, true, 0);
					if(other != null) {
						IMarker marker = project.createMarker(IMarker.PROBLEM);
						if(other.getSource() != null) {
							marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);
							marker.setAttribute(IMarker.MESSAGE, "Language " + other.getName() + " defines another language with the same extension as this project");
						} else {
							marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_WARNING);
							marker.setAttribute(IMarker.MESSAGE, "The extension defined by this project masks language " + other.getName() + " registered by a plugin");
						}
					}
				} catch (CoreException e) {
					e.printStackTrace();
				}
				languageByExtension.put(ret.getExtension(), ret);

				// Retrieving optional extra model extensions for which extraction should be provided
				String extraModelExtensions = ((WorkspaceLanguage)ret).getProperty("extra.model.ext");
				if(extraModelExtensions != null) {
					String parts[] = extraModelExtensions.split(",");
					for(int i = 0 ; i < parts.length ; i++) {
						languageByExtension.put("extraModelExtension:" + parts[i], ret);
					}
				}

				// Retrieving optional model extension that should be used for extraction (instead of default ${dsl.ext}.xmi)
				String modelExtension = ((WorkspaceLanguage)ret).getProperty("model.ext");
				if(modelExtension != null) {
					languageByExtension.put("extraModelExtension:" + modelExtension, ret);
				}
				register(ret);
			} catch(RuntimeException re) {
				// we get here when the project is not valid
			}
		}

		return ret;
	}

	private void register(Language language) {
		for(Iterator i = listeners.iterator() ; i.hasNext() ; ) {
			LanguageRegistryListener registryListener = (LanguageRegistryListener)i.next();
			registryListener.registered(language);
		}
	}
}

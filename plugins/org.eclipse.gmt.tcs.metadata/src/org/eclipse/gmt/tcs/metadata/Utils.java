/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: Utils.java,v 1.3 2008/04/13 21:45:55 fjouault Exp $
 */
package org.eclipse.gmt.tcs.metadata;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.m2m.atl.core.ui.vm.asm.ASMModelWrapper;
import org.eclipse.m2m.atl.drivers.emf4atl.ASMEMFModel;
import org.eclipse.m2m.atl.drivers.emf4atl.ASMEMFModelElement;
import org.eclipse.m2m.atl.engine.MarkerMaker;
import org.eclipse.m2m.atl.engine.vm.ModelLoader;
import org.eclipse.m2m.atl.engine.vm.nativelib.ASMModel;
import org.eclipse.m2m.atl.engine.vm.nativelib.ASMModelElement;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorRegistry;
import org.eclipse.ui.IFileEditorMapping;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.registry.EditorDescriptor;
import org.eclipse.ui.internal.registry.EditorRegistry;
import org.eclipse.ui.internal.registry.FileEditorMapping;

/**
 * @author Fr�d�ric Jouault
 *
 */
public class Utils {
    public static void registerEditorForExtension(final String editorId, final String ext, final boolean isDefault) {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				String dummyFileName = "test." + ext;
				IEditorDescriptor previousDefaultEditorDescriptor = PlatformUI.getWorkbench().getEditorRegistry().getDefaultEditor(dummyFileName);
				String previousDefaultEditorId = (previousDefaultEditorDescriptor == null) ? null : previousDefaultEditorDescriptor.getId();
				actualRegisterEditorForExtension(editorId, ext);
				if(isDefault) {
					PlatformUI.getWorkbench().getEditorRegistry().setDefaultEditor(dummyFileName, editorId);
				} else if(previousDefaultEditorDescriptor != null) {
					PlatformUI.getWorkbench().getEditorRegistry().setDefaultEditor(dummyFileName, previousDefaultEditorId);
				}
			}
		});
    }

    private static void actualRegisterEditorForExtension(String editorId, String ext) {
        IEditorRegistry editorRegistry = PlatformUI.getWorkbench().getEditorRegistry();
        IEditorDescriptor editorDescriptor = editorRegistry.findEditor(editorId);
        if (editorDescriptor != null) {
	        IFileEditorMapping fileEditorMappings[] = editorRegistry.getFileEditorMappings();
	        IFileEditorMapping fileEditorMapping = null;
	        for (int i = 0 ; i < fileEditorMappings.length ; i++) {
	            IFileEditorMapping aFileEditorMapping = fileEditorMappings[i];
	            if(aFileEditorMapping.getExtension().equals(ext)) {
	            	fileEditorMapping = aFileEditorMapping;
	                break;
	            }
	        }
	        
	        if (fileEditorMapping != null) {
	            IEditorDescriptor[] editorDescriptors = fileEditorMapping.getEditors();
	            IEditorDescriptor mappedEditorDescriptor = null;
	            for (int i = 0 ; i < editorDescriptors.length ; i++) {
	                if (editorDescriptors[i].getId().equals(editorId)) {
	                	mappedEditorDescriptor = editorDescriptors[i];
	                    break;
	                }
	            }

	            if(mappedEditorDescriptor == null) {
	            	((FileEditorMapping)fileEditorMapping).addEditor((EditorDescriptor)editorDescriptor);
	            	try {
	            		((EditorRegistry)editorRegistry).setFileEditorMappings((FileEditorMapping[])fileEditorMappings);
	            	} catch(Exception e) {
		            	// an exception will not prevent saving associations
	            	}
	            	((EditorRegistry)editorRegistry).saveAssociations();
	            }
	
	        } else {
	        	fileEditorMapping = new FileEditorMapping(ext);
	            FileEditorMapping[] newFileEditorMappings = new FileEditorMapping[fileEditorMappings.length + 1];
            	((FileEditorMapping)fileEditorMapping).addEditor((EditorDescriptor)editorDescriptor);
	
	            System.arraycopy(fileEditorMappings, 0, newFileEditorMappings, 0, fileEditorMappings.length);
	            newFileEditorMappings[fileEditorMappings.length] = (FileEditorMapping)fileEditorMapping;
	            try {
	            	((EditorRegistry)editorRegistry).setFileEditorMappings((FileEditorMapping[])newFileEditorMappings);
	            } catch(Exception e) {
	            	// an exception will not prevent saving associations
	            }
            	((EditorRegistry)editorRegistry).saveAssociations();
	        }
        }
    }

	public static void save(ModelLoader ml, ASMModel model, IFile file) throws IOException {
		ml.save(model, URI.createPlatformResourceURI(file.getFullPath().toString(), false).toString());
	}

	public static int applyMarkers(IFile file, ASMModel problemModel) throws CoreException {
		return new MarkerMaker().applyMarkers(file, new ASMModelWrapper(problemModel, null));
	}

	public static void save(ASMModel ecoreModel, OutputStream outputStream) throws IOException {
		((ASMEMFModel)ecoreModel).getExtent().save(outputStream, Collections.emptyMap());
	}

	public static void applyMarkers(IFile file, ASMModel pbs, int tabWidth) throws CoreException {
		// we cannot use MarkerMaker.applyMarkers because it assumes a precise IReferenceModel
		Set pbss = pbs.getElementsByType("Problem");
		EObject eos[] = new EObject[pbss.size()];
		int k = 0;
		for(Iterator i = pbss.iterator() ; i.hasNext() ;) {
			ASMModelElement pb = (ASMModelElement)i.next();
			eos[k++] = ((ASMEMFModelElement)pb).getObject();
		}
		new MarkerMaker().resetPbmMarkers(file, eos);
	}
}

/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: LanguageRegistryListener.java,v 1.1 2008/04/01 17:47:40 fjouault Exp $
 */
package org.eclipse.gmt.tcs.metadata;

/**
 * @author Fr�d�ric Jouault
 *
 */
public interface LanguageRegistryListener {

	public void registered(Language language);
}

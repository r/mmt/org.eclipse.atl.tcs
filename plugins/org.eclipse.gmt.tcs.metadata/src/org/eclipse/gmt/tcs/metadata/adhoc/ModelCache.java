/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: ModelCache.java,v 1.1 2008/04/08 15:04:35 fjouault Exp $
 */
package org.eclipse.gmt.tcs.metadata.adhoc;

import java.lang.ref.SoftReference;
import java.util.Map;
import java.util.WeakHashMap;

import org.eclipse.gmt.tcs.metadata.ModelFactory;

/**
 * @author Fr�d�ric Jouault
 *
 */
public abstract class ModelCache {

	private Map modelByModelFactory = null;

	public final synchronized Object getModel(ModelFactory factory) {
		Object ret = null;

		if(!shouldReload()) {
			if(modelByModelFactory != null) {
				SoftReference sr = (SoftReference)modelByModelFactory.get(factory);
				if(sr != null) {
					ret = sr.get();
				}
			}
		}

		if(ret == null) {
			ret = loadModel(factory);
			putModel(factory, ret);
		}

		return ret;
	}

	private void putModel(ModelFactory factory, Object model) {
		if(modelByModelFactory == null) {
			modelByModelFactory = new WeakHashMap();
		}
		modelByModelFactory.put(factory, new SoftReference(model));
	}

	protected abstract Object loadModel(ModelFactory factory);

	protected boolean shouldReload() {
		return false;
	}
}

/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: PluginLanguage.java,v 1.2 2008/04/08 15:06:05 fjouault Exp $
 */
package org.eclipse.gmt.tcs.metadata.adhoc;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.gmt.tcs.extractor.TCSExtractor;
import org.eclipse.gmt.tcs.metadata.Language;
import org.eclipse.gmt.tcs.metadata.LanguageSource;
import org.eclipse.gmt.tcs.metadata.ModelFactory;
import org.eclipse.m2m.atl.dsls.DSLResourceProvider;
import org.eclipse.m2m.atl.dsls.textsource.TextSource;
import org.eclipse.m2m.atl.engine.vm.ASM;
import org.eclipse.m2m.atl.engine.vm.nativelib.ASMModel;

/**
 * 
 * @author Fr�d�ric Jouault
 */
public class PluginLanguage implements Language {

	private static class ModelCacheFromURL extends ModelCache {
		private String metamodelName;
		private URI metamodelURI;
		private String modelName;
		private URL modelURL;

		public ModelCacheFromURL(String metamodelName, URI metamodelURI, String modelName, URL modelURL) {
			this.metamodelName = metamodelName;
			this.metamodelURI = metamodelURI;
			this.modelName = modelName;
			this.modelURL = modelURL;
		}

		protected Object loadModel(ModelFactory factory) {
			Object metamodel = factory.loadMetamodel(metamodelName, metamodelURI);
			Object ret;
			try {
				ret = factory.loadModel(modelName + "-" + metamodelName, metamodel, modelURL.openStream());
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			return ret;
		}
	}

	private String name;
	private String extension;
	private URI metamodelURI;
	private ModelCache metamodelCache = new ModelCache() {
		protected Object loadModel(ModelFactory factory) {
			return factory.loadMetamodel(name, metamodelURI);
		}
	};
	private ModelCache syntaxCache;
	private ModelCache editorCache;
	private ModelCache outlineCache;
	private URL parser;

	public PluginLanguage(String extension, String name, URI metamodel,
			URL syntax, URL editor, URL outline, URL parser) {
		this.extension = extension;
		this.name = name;
		this.metamodelURI = metamodel;
		syntaxCache = new ModelCacheFromURL("TCS", DSLResourceProvider.getDefault().getResource("TCS/Metamodel/TCS.ecore").asEMFURI(), name, syntax);
		editorCache = new ModelCacheFromURL("Editor", DSLResourceProvider.getDefault().getResource("Editor/Metamodel/Editor.ecore").asEMFURI(), name, editor);
		outlineCache = new ModelCacheFromURL("Outline", DSLResourceProvider.getDefault().getResource("Outline/Metamodel/Outline.ecore").asEMFURI(), name, outline);
		this.parser = parser;
	}

	public LanguageSource getSource() {
		return null;	// no associated source
	}

	public Object inject(ModelFactory factory, Object model, TextSource source, Map params) {
		params.put("name", name);
		// TODO: support multiple parserGenerators
		params.put("parserGenerator", "antlr3");
		return TCSInjection.inject(factory, model, getMetamodel(factory), source, params, parser);
	}

	public void extract(ModelFactory factory, Object model, OutputStream out, Map params) {
		Object format = getSyntax(factory);
		params.put("format", format);
		new TCSExtractor().extract((ASMModel)model, out, params);
	}

	private Object getSyntax(ModelFactory factory) {
		return syntaxCache.getModel(factory);
	}

	public Object getEditorModel(ModelFactory factory) {
		return editorCache.getModel(factory);
	}

	public Object getOutlineModel(ModelFactory factory) {
		return outlineCache.getModel(factory);
	}

	public Object getMetamodel(ModelFactory factory) {
		return metamodelCache.getModel(factory);
	}

	public String getName() {
		return name;
	}

	public String getExtension() {
		return extension;
	}

	public ASM getCompiler() {
		return null;
	}
}

/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: Startup.java,v 1.2 2008/06/25 12:22:26 fjouault Exp $
 */
package org.eclipse.gmt.tcs.metadata.adhoc;

import org.eclipse.gmt.tcs.metadata.LanguageRegistry;
import org.eclipse.ui.IStartup;

/**
 * 
 * @author Fr�d�ric Jouault
 */
public class Startup implements IStartup {

	public void earlyStartup() {
		// force traversal of all projects
		LanguageRegistry.getDefault().getLanguage("<dummy>");
		// TODO: listen to workspace changes? For instance:
//		ResourcesPlugin.getWorkspace().addResourceChangeListener(new IResourceChangeListener() {
//			public void resourceChanged(IResourceChangeEvent event) {
//				event.getDelta()...
//			}
//		}, IResourceChangeEvent.POST_CHANGE);
	}
}

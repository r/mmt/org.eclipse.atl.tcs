/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: ModelFactory.java,v 1.2 2008/04/16 11:55:17 fjouault Exp $
 */
package org.eclipse.gmt.tcs.metadata;

/**
 * @author Fr�d�ric Jouault
 *
 */
public interface ModelFactory {

	/**
	 * @param metamodel
	 * @return
	 */
	public Object newModel(String name, Object metamodel);

	public Object loadMetamodel(String name, Object location);

	public Object loadModel(String name, Object metamodel, Object location);

	/**
	 * @param model
	 * @return the metamodel of the model passed as parameter.
	 */
	public Object metamodelOf(Object model);
}

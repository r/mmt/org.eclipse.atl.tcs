/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: ASMModelFactory.java,v 1.2 2008/04/16 11:55:17 fjouault Exp $
 */
package org.eclipse.gmt.tcs.metadata;

import java.io.IOException;
import java.io.InputStream;

import org.eclipse.emf.common.util.URI;
import org.eclipse.m2m.atl.drivers.emf4atl.EMFModelLoader;
import org.eclipse.m2m.atl.engine.vm.AtlModelHandler;
import org.eclipse.m2m.atl.engine.vm.nativelib.ASMModel;

/**
 * @author Fr�d�ric Jouault
 *
 */
public class ASMModelFactory implements ModelFactory {

	private static ModelFactory INSTANCE = new ASMModelFactory();

	public static ModelFactory getDefault() {
		return INSTANCE;
	}

	private EMFModelLoader ml;

	private ASMModelFactory() {
		ml = (EMFModelLoader)AtlModelHandler.getDefault(AtlModelHandler.AMH_EMF).createModelLoader();
	}

	public Object newModel(String name, Object metamodel) {
		return ml.newModel(name, name, (ASMModel)metamodel);
	}

	public Object loadMetamodel(String name, Object location) {
		return loadModel(name, ml.getMOF(), location);
	}

	public Object loadModel(String name, Object metamodel, Object location) {
		try {
			if(location instanceof URI) {
				return ml.loadModel(name, (ASMModel)metamodel, (URI)location);
			} else {
				return ml.loadModel(name, (ASMModel)metamodel, (InputStream)location);			
			}
		} catch(IOException ioe) {
			throw new RuntimeException("Could not load model", ioe);
		}
	}

	public Object metamodelOf(Object model) {
		return ((ASMModel)model).getMetamodel();
	}
}

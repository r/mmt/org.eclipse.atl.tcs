/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: LanguageRegistryListener.java,v 1.2 2008/04/13 21:45:58 fjouault Exp $
 */
package org.eclipse.gmt.tcs.emfresource;

import java.util.Map;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gmt.tcs.metadata.Language;
import org.eclipse.gmt.tcs.metadata.Utils;

/**
 * @author Fr�d�ric Jouault
 *
 */
public class LanguageRegistryListener implements org.eclipse.gmt.tcs.metadata.LanguageRegistryListener {

	public void registered(Language language) {
		String extension = language.getExtension();
		Utils.registerEditorForExtension("org.eclipse.emf.ecore.presentation.ReflectiveEditorID", extension, false);
		Map etfm = Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap();
		etfm.put(extension, new TCSResourceFactoryImpl());
	}

}

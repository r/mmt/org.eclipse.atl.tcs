/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: TCSResourceFactoryImpl.java,v 1.2 2008/04/08 15:11:31 fjouault Exp $
 */
package org.eclipse.gmt.tcs.emfresource;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * 
 * @author Frederic Jouault
 */
public class TCSResourceFactoryImpl implements Resource.Factory {

	public Resource createResource(URI uri) {
		if(uri.scheme() != null) {
			return new TCSResource(uri);
		} else {
			// if there is no scheme, then it may have been created by ant tasks
			return new XMIResourceImpl(uri);
		}
	}

}

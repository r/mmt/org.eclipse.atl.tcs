/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: TCSResource.java,v 1.3 2008/07/04 12:17:22 fjouault Exp $
 */
package org.eclipse.gmt.tcs.emfresource;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.impl.ResourceImpl;
import org.eclipse.gmt.tcs.metadata.ASMModelFactory;
import org.eclipse.gmt.tcs.metadata.Language;
import org.eclipse.gmt.tcs.metadata.LanguageRegistry;
import org.eclipse.gmt.tcs.metadata.ModelFactory;
import org.eclipse.gmt.tcs.metadata.RegularVMLauncher;
import org.eclipse.m2m.atl.drivers.emf4atl.ASMEMFModel;
import org.eclipse.m2m.atl.drivers.emf4atl.EMFModelLoader;
import org.eclipse.m2m.atl.dsls.textsource.TextSource;
import org.eclipse.m2m.atl.engine.vm.AtlModelHandler;
import org.eclipse.m2m.atl.engine.vm.nativelib.AMN;
import org.eclipse.m2m.atl.engine.vm.nativelib.ASMModel;
import org.eclipse.m2m.atl.engine.vm.nativelib.ASMModelElement;

/**
 * 
 * @author Frederic Jouault
 */
public class TCSResource extends ResourceImpl {

	private boolean reportProblems = false;

	public TCSResource(URI uri) {
		super(uri);
	}

	protected void doLoad(final InputStream in, Map arg1) throws IOException {
		URI uri = getURI();
		Map uriMap = this.getURIConverter().getURIMap();
		if(uriMap.containsKey(uri)) {
			uri = (URI)uriMap.get(uri);
		}
		Language language = LanguageRegistry.getDefault().getLanguage(uri.fileExtension());
		if(language != null) {
			Map params = new HashMap();
			EMFModelLoader ml = (EMFModelLoader)AtlModelHandler.getDefault(AtlModelHandler.AMH_EMF).createModelLoader();
			ModelFactory factory = ASMModelFactory.getDefault();
			ASMEMFModel metamodel = (ASMEMFModel)language.getMetamodel(factory);
			ASMModel pbs = ml.newModel("pbs", "pbs.xmi", ml.getBuiltInMetaModel("Problem.ecore"));
			params.put("problems", pbs);
			params.put("vmLauncher", new RegularVMLauncher());
			try {
				language.inject(factory,new ASMEMFModel("model.xmi", this, metamodel, true, ml) {}, new TextSource() {
					public InputStream openStream() throws IOException {
						return in;
					}			
				}, params);
			} catch (Exception e) {
				IOException ioe = new IOException("injection error");
				ioe.initCause(e);
				throw ioe;
			}
			int nbErrors = 0;
			if(reportProblems) {
				for(Iterator i = pbs.getElementsByType("Problem").iterator() ; i.hasNext() ; ) {
					ASMModelElement ame = (ASMModelElement)i.next();
					String severity = AMN.getString(ame, "severity");
					List list = null;
					if("error".equals(severity)) {
						list = getErrors();
						nbErrors++;
					} else if("warning".equals(severity)) {
						list = getWarnings();
					}
					if(list != null) {
						final String message = AMN.getString(ame, "description");
						String location = AMN.getString(ame, "location");
						String parts[] = location.split("-")[0].split(":");
						final int line = Integer.parseInt(parts[0]);
						final int column = Integer.parseInt(parts[1]);
						list.add(new Diagnostic() {
							public int getColumn() {
								return column;
							}
	
							public int getLine() {
								return line;
							}
	
							public String getLocation() {
								return getURI().toString();
							}
	
							public String getMessage() {
								return message;
							}
						});
					}
				}
				if(nbErrors == 0) {
					getWarnings().clear();	// because the EMF editor fails when there are warnings
				}
			}
		} else {
			throw new RuntimeException("Unknown extension: " + uri.fileExtension());
		}
	}

	protected void doSave(OutputStream arg0, Map arg1) throws IOException {
//		String fileName = uri.lastSegment();
//		String parts[] = fileName.split("\\.");
//		if(parts.length > 2) {
//			if(parts[])
//		}
		throw new RuntimeException("cannot save");
	}

}

/**
 * Copyright (c) 2008 INRIA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     INRIA - initial API and implementation
 *
 * $Id: CompletionInformation.java,v 1.3 2008/06/25 12:27:04 fjouault Exp $
 */
package org.eclipse.gmt.tcs.injector;

import java.util.List;

/**
 * 
 * @author Fr�d�ric Jouault
 *
 */
public interface CompletionInformation {

	public int getOffset();
	public List getProposals();
	public void setPrefix(String prefix);
	public String getPrefix();
}
